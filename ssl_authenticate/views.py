from django.contrib.auth.models import User
from django.contrib import auth
from django.contrib.auth.views import logout
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse

from django.shortcuts import *
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.core.context_processors import csrf

from django.contrib.auth.forms import UserCreationForm
import filestorage.views
import logging

logger = logging.getLogger(__name__)

def certificate_login(request):

    logger = logging.getLogger(__name__)
    logger.info("trying certificate login...")
    c = {}
    c.update(csrf(request))

    if request.user.is_authenticated():
        logger.info("user is authenticated")

        try:
            logger.info("user certificate is authenticated...{0}".format(request.META['SSL_CLIENT_S_DN']))
            request.session['dn'] = request.META['SSL_CLIENT_S_DN']
            return render(request, 'accounts/loggedin.html')

        except KeyError:
            logger.info("user authenticated, but not-using certificate")
            request.session['dn'] = "Non Certificate Login"

            return render(request, 'accounts/loggedin.html')

    else:
        logger.error("user was not authenticated, going for simple login")
        return HttpResponseRedirect(reverse('simplelogin'))

def simple_login(request):
        # Like before, obtain the context for the user's request.
    logger = logging.getLogger(__name__)
    logger.info("request.META...", dir(request.META))

    c = {}
    c.update(csrf(request))

    # If the request is a HTTP POST, try to pull out the relevant information.
    if request.method == 'POST':
        # Gather the username and password provided by the user.
        # This information is obtained from the login form.
        username = request.POST['username']
        password = request.POST['password']

        # Use Django's machinery to attempt to see if the username/password
        # combination is valid - a User object is returned if it is.
        user = authenticate(username=username, password=password)

        # If we have a User object, the details are correct.
        # If None (Python's way of representing the absence of a value), no user
        # with matching credentials was found.
        if user:
            logger.info("user {0} is authenticated".format(username))
            # Is the account active? It could have been disabled.
            if user.is_active:
                logger.info("and user is active")
                # If the account is valid and active, we can log the user in.
                # We'll send the user back to the homepage.
                login(request, user)
                return HttpResponseRedirect(reverse("loggedin"))
            else:
                logger.info("but user is inactive")
                # An inactive account was used - no logging in!
                return HttpResponse("Your account is disabled or not active.")
        else:
            # Bad login details were provided. So we can't log the user in.
            logger.info( "Invalid login details: {0}, {1}".format(username, password))
            return HttpResponse("Invalid login details supplied.")

    # The request is not a HTTP POST, so display the login form.
    # This scenario would most likely be a HTTP GET.
    else:
        # No context variables to pass to the template system, hence the
        # blank dictionary object...
        return render(request, 'accounts/simple_login.html')


def logged_in(request):

    '''Save Users group memberships to the session to prevent multiple DB calls'''

    request.user_groups = filestorage.views.get_group_choices(request.user)

    request.session.modified = True

    return render(request, 'accounts/loggedin.html')

def logout(request):
    auth.logout(request)
    return render(request, 'accounts/logout.html')


def invalid_login(request):
    return render(request, 'accounts/invalid_login.html')

def register_user(request):
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('accounts/register_success')

    args = {}
    args.update(csrf(request))

    args['form'] = UserCreationForm()

    return render('accounts/register.html', args)

def register_success(request):
    return render_to_response('accounts/register_success')

# @login_required
# def view_profile(request):
#
#     logger.info("request dictionary : %s", dir(request.META.viewkeys))
#     url = request.user.get_profile().firstname
#     #logger.info("user profile - %s", url)
#
#     return url


#from rango.forms import UserForm, UserProfileForm

def profile(request):
    return render(request, 'accounts/loggedin.html')

# def register(request):
#     # Like before, get the request's context.
#     context = RequestContext(request)
#
#     # A boolean value for telling the template whether the registration was successful.
#     # Set to False initially. Code changes value to True when registration succeeds.
#     registered = False
#
#     # If it's a HTTP POST, we're interested in processing form data.
#     if request.method == 'POST':
#         # Attempt to grab information from the raw form information.
#         # Note that we make use of both UserForm and UserProfileForm.
#         user_form = UserForm(data=request.POST)
#         profile_form = UserProfileForm(data=request.POST)
#
#         # If the two forms are valid...
#         if user_form.is_valid() and profile_form.is_valid():
#             # Save the user's form data to the database.
#             user = user_form.save()
#
#             # Now we hash the password with the set_password method.
#             # Once hashed, we can update the user object.
#             user.set_password(user.password)
#             user.save()
#
#             # Now sort out the UserProfile instance.
#             # Since we need to set the user attribute ourselves, we set commit=False.
#             # This delays saving the model until we're ready to avoid integrity problems.
#             profile = profile_form.save(commit=False)
#             profile.user = user
#
#             # Did the user provide a profile picture?
#             # If so, we need to get it from the input form and put it in the UserProfile model.
#             if 'picture' in request.FILES:
#                 profile.picture = request.FILES['picture']
#
#             # Now we save the UserProfile model instance.
#             profile.save()
#
#             # Update our variable to tell the template registration was successful.
#             registered = True
#
#         # Invalid form or forms - mistakes or something else?
#         # Print problems to the terminal.
#         # They'll also be shown to the user.
#         else:
#             print user_form.errors, profile_form.errors
#
#     # Not a HTTP POST, so we render our form using two ModelForm instances.
#     # These forms will be blank, ready for user input.
#     else:
#         user_form = UserForm()
#         profile_form = UserProfileForm()
#
#     # Render the template depending on the context.
#     return render_to_response(
#             'rango/register.html',
#             {'user_form': user_form, 'profile_form': profile_form, 'registered': registered},
#             context)
