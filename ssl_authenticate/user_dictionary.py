#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2013 SSH Communication Security Corporation.
# All rights reserved.
# This software is protected by international copyright laws.
#


"""
Functions for handling certificates from SAGrid CA (https://security.sanren.ac.za/CA/)
"""
import logging


def _dictify_dn(dn):
    logger = logging.getLogger(__name__)
    
    """
    Splits the string at the / and = and returns a dictionary, must be made compatible with Apache 2.2
    """
    logger.info("user dn = " + dn)
    try:
        user_profile = dict(x.split('=') for x in dn.split(',') if '=' in x)
    except ValueError:
        user_profile = dict(x.split('=') for x in dn.split('/') if '=' in x)
        
    logger.info("dictified user profile - {0}".format(user_profile))

    return user_profile 

def _extract_email(user_certificate):
    
    import M2Crypto          
    logger = logging.getLogger(__name__)
        
    certificate_object = M2Crypto.X509.load_cert_string(user_certificate)
    extension_total = certificate_object.get_ext_count()
    #logger.info ("number of SAN extensions - %s", extension_total)
    
    extension_pos = 0
    email_field = []
    while extension_pos < extension_total:
        #logger.info("extension_pos = %s, extension_total %s", extension_pos, extension_total)
        email_field.append(str(certificate_object.get_ext_at(extension_pos).get_value()))
        extension_pos +=1 
        #logger.info(email_field)
        
    #logger.info("SAN field - %s", email_field)
        
    for fields in email_field:    
        #logger.info("email_fields field - %s", fields)
        if '@' in str(fields):
            
            email = fields.split(":")
            logger.info("user email - {0}".format(email[1]))
        else:
            continue
        return email[1]
        
def user_dict_from_dn(dn, user_certificate):
        
    '''
    Compile a dictionary of the username and email from the DN and X509 Certificate.
    '''
    
    logger = logging.getLogger(__name__)    
        
    d = _dictify_dn(dn)    
    user_email = _extract_email(user_certificate)
    
    #The users email address is set in "Subject Alternative Name" under Extensions in current CA format
    # Need to control for a different number of Extensions, this might change 
    # Would prefer something a little neater without a counter    
    ret = dict()    
        
    ret['username'] = d.get('CN').replace(" ", "").lower()
    fullname = d.get('CN').split(" ")
    number_of_names = len(fullname)
    ret['first_name'] = fullname[0]
    ret['last_name'] = fullname[number_of_names-1]
    ret['email'] = user_email
    logger.info("username dictionary - {0}".format(str(ret)))
            
    return ret
