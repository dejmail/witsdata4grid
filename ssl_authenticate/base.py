#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright © 2013 SSH Communication Security Corporation.
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN logging.basicConfig()ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#

import logging
from django.conf import settings
from django.contrib.auth import login, authenticate
from django.core.exceptions import ImproperlyConfigured
from user_dictionary  import user_dict_from_dn
from django.http import HttpResponseRedirect
logging.basicConfig(level=logging.INFO)


try:
    from django.contrib.auth import get_user_model

    User = get_user_model()
except ImportError:
    from django.contrib.auth.models import User

logger = logging.getLogger(__name__)

class SSLClientAuthBackend(object):

    @staticmethod
    def authenticate(request):
        logger = logging.getLogger(__name__)

        # Have not used the authors original implementation of the import. unsure
        # of when a circular import would occur.

        #_package_name, _module_name, _function_name = settings.USER_DATA_FN
        #_module = imp.load_module(_module_name, *imp.find_module(_package_name))
        #USER_DATA_FN = getattr(getattr(_module, _module_name), _function_name)

        if not request.is_secure():
            logger.debug("insecure request")
            return None
        authentication_status = request.META['SSL_CLIENT_VERIFY']
        logger.info("authentication status = {0}".format(authentication_status))
        if (authentication_status != "SUCCESS" or
                                'SSL_CLIENT_VERIFY' not in request.META):
            logger.error(
                "HTTP_X_SSL_AUTHENTICATED marked failed or "
                "HTTP_X_SSL_USER_DN "
                "header missing")
            return None

        dn = str(request.META['SSL_CLIENT_S_DN'])
        request.session['dn'] = dn
        logger.info("DN from Apache - {0}".format(dn))

        user_certificate = str(request.META['SSL_CLIENT_CERT'])

        # If the request has passed through any proxies, then this might be a comma-separated list
        # of IP addresses, e.g., "12.345.67.89,23.456.78.90". Not sure how this is presented below.

        if 'HTTP_X_FORWARDED_FOR' not in request.META:
            logger.info("user_ip REMOTE_ADDR = %s", request.META['REMOTE_ADDR'])
            request.META['HTTP_X_FORWARDED_FOR'] = request.META['REMOTE_ADDR']
            request.session.modified = True
        else:
            logger.info("user_ip HTTP_X_FORWARDED_FOR...{0}".format(request.META['HTTP_X_FORWARDED_FOR']))

        if request.META['HTTP_X_FORWARDED_FOR']:
            logger.info('request.META["HTTP_X_FORWARDED_FOR"]...{0}'.format(request.META['HTTP_X_FORWARDED_FOR']))

        user_data = user_dict_from_dn(dn, user_certificate)
        #logger.info(user_data)
        username = user_data.get('username')
        #logger.info("username is %s", username)

        try:
            user = User.objects.get(username=username)
            logger.info("it seems the username exists")
        except User.DoesNotExist:
            logger.info("user {0} not found".format(username))
            if settings.AUTOCREATE_VALID_SSL_USERS:
                user = User(**user_data)
                logger.info("user %s created...{0}".format(username))
                user.save()

            else:
                return None
        if not user.is_active:
            logger.warning("user {0} inactive".format(username))
            return None
        logger.info("user {0} authenticated using a certificate issued to "
                    "{1}".format(username, dn))

        return user

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return "User does not exist"

class SSLClientAuthMiddleware(object):

    def process_request(self, request):

        if not hasattr(request, 'user'):
            raise ImproperlyConfigured()

        if request.user.is_authenticated():
            return
        user = authenticate(request=request)

        if user is None or not user.is_authenticated():
            return
        if int(request.META.get('HTTP_X_REST_API', 0)):
            request.user = user
            logger.debug("REST API call, not logging user in")
        else:
            logger.info("Logging user in")
            login(request, user)
