#!/usr/bin/env python
import os
import sys
import site
import socket

if __name__ == "__main__":

    if socket.gethostname().startswith('ray'):
        # Add the site-packages of the chosen virtualenv to work with
        sys.path.insert(0, '/opt/python/witsdata4grid/')
        site.addsitedir('/opt/python/witsdata4grid/env/lib/python2.6/site-packages/')
               
        # Activate your virtual env
        activate_env=os.path.expanduser("/opt/python/witsdata4grid/env/bin/activate_this.py")
        execfile(activate_env, dict(__file__=activate_env))
    
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings.staging")
        #logger.debug(DJANGO_SETTINGS_MODULE)
        
    elif socket.gethostname() == 'blueice':
        # Add the site-packages of the chosen virtualenv to work with
        sys.path.insert(0, '/opt/code_home/django/witsdata4grid/')
        site.addsitedir('/opt/code_home/django/witsdata4grid/env/lib/python2.6/site-packages/')
    
        # Activate your virtual env
        activate_env=os.path.expanduser("/opt/code_home/django/witsdata4grid/env/bin/activate_this.py")
        execfile(activate_env, dict(__file__=activate_env))
    
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings.local")
        #logger.debug(DJANGO_SETTINGS_MODULE)
        
    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)

#logger = logging.getLogger(__name__)


# We defer to a DJANGO_SETTINGS_MODULE already in the environment. This breaks
# if running multiple sites in the same mod_wsgi process. To fix this, use
# mod_wsgi daemon mode with each site in its own daemon process, or use
