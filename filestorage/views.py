from django.conf import settings
import os

from django.contrib.auth.decorators import login_required
from django.contrib.auth import models
from django.contrib.auth.models import User, Group
from django.db.models import Q

from django.core.urlresolvers import reverse
from django.core.validators import URLValidator
from django.core.exceptions import (ValidationError, ObjectDoesNotExist)
from django.core.paginator import (Paginator, EmptyPage, PageNotAnInteger)
from django.core.files.uploadhandler import (FileUploadHandler, UploadFileException)

import requests
from django.shortcuts import *

from django.http import HttpResponse, HttpResponseRedirect
from django.template.response import TemplateResponse
import json

from celery import Celery
from settings.celery import app


from django import forms
from filestorage.forms import StoredFileMetadataForm, FileForm, UploadFileForm
from models import StorageElementBasePath, FileStorageParameters, VirtualOrganisation, FilesSE, FileVO, StoredFileMetadata


from lfcinteract.tasks import copy_file
from proxy.tasks import check_proxy_health


from django.core.context_processors import csrf
import logging
from django.http.response import HttpResponseServerError

@login_required
def alldata(request):

    '''
    Need to make this viewing only by superuser
    '''

    return render(request, 'filestorage/alldata.html',
                  {'data' : StoredFileMetadata.objects.all()})

@login_required
def incomplete_data(request):

    '''
    For when datasets are saved but there is no SE base path specified
    '''

    logger = logging.getLogger(__name__)
    args = {}

    #args['data'] = FileStorageParameters.objects.filter(se_base_path__isnull=True).select_related('filemetadata').prefetch_related('se_base_path', 'file_vo')
    args['data'] = StoredFileMetadata.objects.filter(Q(filestorageparameters__isnull=True) | Q(filestorageparameters__file_successfully_uploaded_to_staging=False)).select_related('filemetadata')


    logger.info("# of incomplete datasets...{0}".format(len(args['data'])))
    logger.info(args['data'].values())

    if len(args['data']) <= 25:
        paginator = Paginator(args['data'], len(args['data']))
    else:
        paginator = Paginator(args['data'], 25) # Show 25 datasets per page

    logger.info("loading {0} datasets into paginator".format(len(args['data'])))

    page = request.GET.get('page')

    try:
        datasets = paginator.page(page)
    except PageNotAnInteger, e:
        logger.error("Exception = {0}".format(e))
        try:
            datasets = paginator.page(1)
        # If page is not an integer, deliver first page.
        #datasets = paginator.page(1)
        except ZeroDivisionError:
            paginator.page(0)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        logger.error("Exception = {0}".format(e))
        datasets = paginator.page(paginator.num_pages)

    logger.info("number of datasets in Queryset...{0}".format(len(args['data'])))

    return render(request, 'filestorage/incomplete.html', {"datasets" : datasets})

@login_required
def choose_data(request):
    logger = logging.getLogger(__name__)

    if request.method == 'GET':
        try:
            selected_pk = request.GET.getlist("selectdata")
            logger.info("user selected dataset...{0}".format(selected_pk[0]))
            url = reverse('datadisplay', args=[selected_pk[0]])
        except IndexError:
            url = Http404

    return HttpResponseRedirect(url)

@login_required
def edit_data(request, dataset):
    logger = logging.getLogger(__name__)

    try:
        selected_metadata = StoredFileMetadata.objects.get(pk=dataset)
    except ObjectDoesNotExist:
        return HttpResponseRedirect(reverse('fileupload'))

    logger.info("dataset {0} is...{1}".format(dataset, selected_metadata))

    form = StoredFileMetadataForm(instance=selected_metadata)
    logger.info("instantiating metadata form with bound data")

    if request.method == 'POST':
        if form.is_valid():
            logger.info("metadata form is valid")
            data = form.save(commit=False)
            data.creator = request.user

            #logger.info("metadata pk made available - %s", data.pk)
            data.save()

            logger.info("fields in data form instance...{0}".format(data.pk))
            request.session['metapk'] = data.pk

            #url = reverse('fileupload')
            #logger.info('fileupload reverse URL = %s', url)
            #return HttpResponseRedirect(url)

    args = {}
    args.update(csrf(request))

    args['form'] = form

        #url = reverse('datacreate')
        #logger.info('reversing name URL "datacreate" - %s', url)

    return render(request, 'filestorage/editdata.html', args)

@login_required
def group_data(request):

    '''
    User only views data they have created or have access to by Group.
    '''

    logger = logging.getLogger(__name__)

    args = {}
    args['data'] = FileStorageParameters.objects.all().select_related('filemetadata').prefetch_related('se_base_path', 'file_vo')

    paginator = Paginator(args['data'], 25) # Show 25 datasets per page

    logger.info("loading 25 datasets into paginator...{0}".format(paginator))

    page = request.GET.get('page')
    try:
        datasets = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        datasets = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        datasets = paginator.page(paginator.num_pages)

    logger.info("number of datasets in Queryset...{0}".format(len(args['data'])))

    return render(request,'filestorage/alldata.html', {"datasets": datasets})

@login_required
def data_display(request, data_id):

    '''
    User only views data they have created or have access to by Group.
    '''
    logger = logging.getLogger(__name__)
    logger.info("selected data record #...{0}".format(data_id))
    args={}
    try:
        # need to forward to file upload url
        args['data'] = FileStorageParameters.objects.get(pk=data_id)

        #.select_related('filemetadata').prefetch_related('se_base_path', 'file_vo')
    except ObjectDoesNotExist:
        logger.error('dataset incomplete - capturing file upload options')
        request.session['metapk'] = data_id
        url = reverse('fileupload')
        return HttpResponseRedirect(url)

    return render(request, 'filestorage/data.html', {'data': args['data']})


#     logger = logging.getLogger(__name__)
#     language = 'en-gb'
#     session_language = 'en-gb'
#
#     if 'lang' in request.COOKIES:
#         language = request.COOKIES['lang']
#
#     if 'lang' in request.session:
#         session_language = request.session['lang']
#
#     args = {}
#     args.update(csrf(request))
#
#     args['data'] = CurrentData.objects.all()
#     args['language'] = language
#     args['session_language'] = session_language
#     logger.debug(args['data'])
#     return render_to_response('data.html', {'datalist' : args})


@login_required
def create(request):

    logger = logging.getLogger(__name__)
    form = StoredFileMetadataForm(request.POST or None)

    if request.POST:
        if form.is_valid():
            logger.info("metadata form is valid")

            data = form.save(commit=False)

            data.creator = request.user

            #logger.info("metadata pk made available - %s", data.pk)
            data.save()

            logger.info("fields in data form instance..{0}".format(data.pk))
            request.session['metapk'] = data.pk

            url = reverse('fileupload')
            logger.info('fileupload reverse URL = {0}'.format(url))
            return HttpResponseRedirect(url)
        #('/witsdata4grid/data/fileupload/')
    #else:
    #    form = StoredFileMetadataForm(request.POST or None)
            #return HttpResponseRedirect(reverse('fileupload'))

    args = {}
    args.update(csrf(request))

    args['form'] = form

    url = reverse('datacreate')
    logger.info('reversing name URL "datacreate"...{0}'.format(url))

    return render(request, 'filestorage/create_newdata.html', args)

# @login_required
# def gridbox_request(request_url):
#
#     logger = logging.getLogger(__name__)
#
#     logger.info("Gridbox Server URL...%s",settings.GRIDBOX_URL)
#
#     #headers = {'x-auth-ip' : '146.141.177.54'}
#     #params = {'authip' : '146.141.177.54'}
#     book_upload_space = requests.get(settings.GRIDBOX_URL + request_url, params=params, headers=headers)
#     #book_upload_space = requests.put('127.0.0.1/webdav' + request_url)
#     logger.info("Gridbox fields...%s", dir(book_upload_space))
#     logger.info("Gridbox redirect url...%s", book_upload_space.url)
#
#     logger.info("Gridbox status code...%s", book_upload_space.status_code)
#
#     return book_upload_space

#@login_required
def reserve_fileupload_space(request):
#(request, *args, **kwargs):
    '''


    URL must be configured as follows "/upload/<vo>/<filename>/<se>/<path:path>"
    '''

    logger = logging.getLogger(__name__)

    virtual_organisation = get_vo_choices(request.user)
    project = get_group_choices(request.user)
    location = get_location_choices()

    form = FileForm(request.POST, virtual_organisation=virtual_organisation, location=location, project=project)

    if request.POST:
        logger.info('request.POST form dir...{0}'.format(request.POST))
        if form.is_valid():
            new_file = form.save(commit=False)

            logger.info("filename...{0}".format(form.cleaned_data['filename']))
            logger.info("se_base_path...{0}".format(form.cleaned_data['se_base_path']))
            logger.info("file_vo...{0}".format(form.cleaned_data['file_vo']))
            logger.info("project...{0}".format(form.cleaned_data['project']))

            stored_metadata = request.session['metapk']
            logger.info("File metadata ID from session...{0}".format(stored_metadata))
            new_file.filemetadata = StoredFileMetadata.objects.get(id=stored_metadata)
            new_file.file_group = Group.objects.get_by_natural_key((form.cleaned_data['project']))

            logger.info("group object selected...{0}".format(type(new_file.file_group)))

            new_file.save()

            se_assigned = StorageElementBasePath.objects.get(location=form.cleaned_data['se_base_path'])
            filese = FilesSE.objects.create(file_info=new_file, se_info=se_assigned)
            vo_assigned = VirtualOrganisation.objects.get(virtual_organisation=form.cleaned_data['file_vo'])
            filevo = FileVO.objects.create(file_info=new_file, vo_info=vo_assigned)

            # https://se.core.wits.ac.za/dpm/core.wits.ac.za/home/sagrid/

            url_parts = ['https://',
                         se_assigned.storage_element,
                         se_assigned.base_path,
                         "/",
                         form.cleaned_data['file_vo'],
                         "/",
                         'witsdata4grid/',
                         form.cleaned_data['project'],
                         "/",
                         form.cleaned_data['filename']]

            requested_url = "".join(url_parts)

            # can pass a list of schemes to the validator - ['http', 'https', 'ftp', 'ftps']
            val = URLValidator()
            try:
                val(requested_url)
                logger.info('requested upload url is properly formed')
            except ValidationError, e:
                logger.error('the requested upload url is malformed')

            # return a json data object of reponse code and url (if successful)
            #gridbox_response = gridbox_request(request, form.cleaned_data['filename'])
            #gridbox_response = json.loads(gridbox_response.text)
            async_check_robot_proxy = check_proxy_health.delay(vo_assigned.virtual_organisation)

            request.session['uploadurl'] = requested_url
            logger.info("requested URL...{0}".format(request.session['uploadurl']))
            request.session.modified = True

            return HttpResponseRedirect(reverse('upload'))
        #(request, 'filestorage/upload.html')

    args = {}
    args.update(csrf(request))

    args['form'] = form

    return render(request, 'filestorage/reserve_upload_space.html', args)

#@login_required
def file_segment_upload(request):
    logger = logging.getLogger(__name__)
    response_data = {}

    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)

        #logger.info("form fields...%s", form.fields)
        logger.info("is form valid...{0}".format(form.is_valid()))
        #logger.info("dir of request.FILES...%s", dir(request.FILES))

        if form.is_multipart():
            logger.info("file upload request form is multipart")
            handle_uploaded_file(request, request.POST['uploadurl'], request.FILES['uploadfile'])
            response_data['status'] = "success"
            return HttpResponseRedirect('/success/')
        else:
            logger.info(dir(form))
            logger.info("form is not multipart")
            response_data['status'] = 'error'
            response_data['result'] = 'an error occurred with the ajax request'

    elif request.method == "GET":
        incoming_details = request.GET
        logger.info(incoming_details)
        form = UploadFileForm()
    else:

#         response_data['status'] = 'error'
#         response_data['result'] = 'an error occurred with the ajax request'

         form = UploadFileForm()
    return render(request, 'filestorage/upload_in_segments.html', {'form': form})

#@login_required
def file_upload(request):
    logger = logging.getLogger(__name__)
    response_data = {}


    #redirect_url = request.session['uploadurl']
    #logger.info("upload redirect url sent to template...%s", redirect_url)
    logger.info("request POST...{0}".format(request.POST))

    if request.method == 'POST':
        logger.info("request POST...{0}".format(request.POST))
        #logger.info("request DIR...%s", dir(request))
    #return HttpResponse(message)
#     if request.method == 'POST':

        form = UploadFileForm(request.POST, request.FILES)

        #logger.info("form fields...%s", form.fields)
        logger.info("is form valid...{0}".format(form.is_valid()))
        #logger.info("dir of request.FILES...%s", dir(request.FILES))

        if form.is_multipart():
            logger.info("file upload request form is multipart")
            handle_uploaded_file(request, request.session['uploadurl'], request.FILES['uploadfile'])
            response_data['status'] = "success"
            response_data['result'] = "File has been uploaded"
            capture_file_upload_to_db(request)
            return HttpResponse(response_data)
        else:
            logger.info(dir(form))
            logger.info("form is not multipart")
            response_data['status'] = 'error'
            response_data['result'] = 'an error occurred with the ajax request'
            form = UploadFileForm()
    else:
#         response_data['status'] = 'error'
#         response_data['result'] = 'an error occurred with the ajax request'

        form = UploadFileForm()
#        return HttpResponse(json.dumps(response_data))
#     args = {}
#     args.update(csrf(request))
#     args['redirect'] = redirect_url
#     args['content_type'] = "application/json"

    return render(request, 'filestorage/upload.html', {'form': form})
#render(request, "filestorage/upload.html")

def capture_file_upload_to_db(request):
    logger = logging.getLogger(__name__)

    metapk = request.session.get('metapk')
    try:
        upload_staged = FileStorageParameters.objects.get(pk=metapk)
        upload_staged.file_successfully_uploaded_to_staging = True
        upload_staged.save()
        logger.info("file upload to staging captured")
    except Exception, e:
        return 'error, file staging not captured in db...{0}'.format(e)

#@login_required
def handle_uploaded_file(request, uploadurl, file, path=''):

    logger = logging.getLogger(__name__)
    logger.info("writing file from POST")

    filename = file._get_name()
    logger.info("filename is...{0}".format(filename))
    logger.info("SITE_ROOT is...{0}".format(settings.SITE_ROOT))
    logger.info("DJANGO_ROOT is...{0}".format(settings.DJANGO_ROOT))
    logger.info("MEDIA_ROOT is...{0}".format(settings.MEDIA_ROOT))
    uploaded_file_path = str(settings.MEDIA_ROOT + "/" + filename)
    #logger.info("intended destination...{0}".format(uploaded_file_path))
    local_destination = open(os.path.join(settings.MEDIA_ROOT, filename), 'wb+')
    logger.info("destination being written to...{0}".format(local_destination))
    for chunk in file.chunks():
        local_destination.write(chunk)
    local_destination.close()
    logger.info("file upload completed")
    metapk = request.session.get('metapk')
    #capture_file_upload_to_db(request)

    # start the celery process under tasks.py to copy the file to the SE.

    async_upload_result = copy_file.apply_async(kwargs={'uploadurl': uploadurl, 'uploaded_file_path' : uploaded_file_path, 'metapk' : metapk})
        #async_upload_result.copy_file.apply_async()
    #async_upload_result = 0

    #async_upload_result = 'task temp'
    #.delay(source=uploaded_file_path, destination=uploadurl)
    #tasks.copy_file(source=local_destination, destination=upload_url)
    return HttpResponse(async_upload_result)

#@login_required
def upload_success(request):
    logger = logging.getLogger(__name__)
    logger.info("file successfully uploaded")
    return render(request, 'filestorage/success.html')

@login_required
def file_download(request, dataset):

    '''

    Get the file the user wants to download. Generate the request with Gridbox, and pass
    this on to the user.

    '''

    logger = logging.getLogger(__name__)

    selected_metadata = StoredFileMetadata.objects.get(pk=dataset)
    logger.info("dataset %s is...{0}".format(dataset, selected_metadata))

    #form = StoredFileMetadataForm(instance=selected_metadata)
    logger.info("instantiating metadata form with bound data")

    if request.method == 'POST':
        if form.is_valid():
            logger.info("metadata form is valid")
            data = form.save(commit=False)
            data.creator = request.user

            #logger.info("metadata pk made available - %s", data.pk)
            data.save()

            logger.info("fields in data form instance- {0}".format(data.pk))
            request.session['metapk'] = data.pk

            #url = reverse('fileupload')
            #logger.info('fileupload reverse URL = %s', url)
            #return HttpResponseRedirect(url)

    args = {}
    args.update(csrf(request))

    args['form'] = form

        #url = reverse('datacreate')
        #logger.info('reversing name URL "datacreate" - %s', url)

    return render(request, 'filestorage/editdata.html', args)
    #return render(request, 'filestorage/download.html', {"datasets": datasets})
#
# # @login_required
# # def upload_file_to_reserved_space(request):
# #
# #     '''
# #     Depending on the result from request to reserve an upload space, the reserved URL which was
# #     returned in JSON is then sent to a user via a separate page/alert to which they can upload.
# #     '''
# #
# #     form = FileForm(request.POST or none, request.FILES or None)
# #     if form.is_valid():
# #         doX()
# #         return redirect('')
# #     #return render(request, template_name, {'form': form})
# #     return render(request)



def get_vo_choices(user):

    logger = logging.getLogger(__name__)
    vo_list = VirtualOrganisation.objects.filter(uservo__username=user).values_list('virtual_organisation', flat=True)

    logger.info(type(vo_list))
    vo_list = [(x,x) for x in vo_list]

    logger.info("user belongs to virtual organisations... = {0}".format(vo_list))

    return vo_list

def get_group_choices(user):

    logger = logging.getLogger(__name__)
    groups_list = Group.objects.filter(user__username=user).values_list('name', flat=True)
    groups_list = [(x,x) for x in groups_list]
    logger.info("user belongs to groups... = {0}".format(groups_list))

    return groups_list

def get_location_choices():

    logger = logging.getLogger(__name__)
    institute_list = StorageElementBasePath.objects.all().values_list('location', flat=True)
    institute_list = [(x,x) for x in institute_list]
    logger.info("institute choices... = {0}".format(institute_list))
    return institute_list
