from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User, Group
from celery.worker.strategy import default

class StoredFileMetadata(models.Model):

    title = models.CharField(max_length=150)
    description = models.TextField(max_length=750)
    subject = models.CharField(max_length=25)
    publisher = models.CharField(max_length=20)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    type = models.CharField(max_length=20)
    format = models.CharField(max_length=20)
    identifier = models.CharField(max_length=20)
    source = models.CharField(max_length=20)
    language = models.CharField(max_length=10)
    relation = models.CharField(max_length=20)
    coverage = models.CharField(max_length=20)
    rights = models.CharField(max_length=20)

    creator = models.ForeignKey(User)
    #file = models.OneToOneField('FileStorageParameters', blank=False)

    class Meta: # default sorting order when data retrieved
        ordering = ['title']

    def __unicode__(self):
        return str(self.title)

class FileStorageParameters(models.Model):

    filemetadata = models.OneToOneField(StoredFileMetadata)
    filename = models.CharField(max_length=255)
    se_base_path = models.ManyToManyField('StorageElementBasePath', through='FilesSE')
    file_vo = models.ManyToManyField('VirtualOrganisation', through='FileVO')
    file_group = models.ForeignKey(Group)
    file_successfully_moved_to_se = models.BooleanField(default=False)
    file_successfully_uploaded_to_staging = models.BooleanField(default=False)

    def __unicode__(self):
        return str(self.filename)

#     @staticmethod
#     def get_users_groups():
#         return Group.objects.all().filter(user__username=user)

class FilesSE(models.Model):

    '''Linker table to connect Storage Element information with File Information'''

    file_info = models.ForeignKey(FileStorageParameters)
    se_info = models.ForeignKey('StorageElementBasePath')

class StorageElementBasePath(models.Model):

    #filename = models.ManyToManyField('FileStorageParameters', through='FilesSE')
    location = models.CharField(max_length=50)
    storage_element = models.CharField(max_length=255)
    base_path = models.CharField(max_length=255)

    def __unicode__(self):
        return str(self.storage_element)

class FileVO(models.Model):

    ''' Linker table to connect the File Information with the Virtual Organisation '''

    file_info = models.ForeignKey(FileStorageParameters)
    vo_info = models.ForeignKey('VirtualOrganisation')

    def __unicode__(self):
        return str(self.file_info)


class VirtualOrganisation(models.Model):

    virtual_organisation = models.CharField(max_length=255)
    user_id = models.ManyToManyField(User, through='UserVO')

    def __unicode__(self):
        return str(self.virtual_organisation)

class UserVO(models.Model):

    ''' Linker table to connect the Virtual Organisation with the User Profile '''

    username = models.ForeignKey(User)
    virtual_organisation = models.ForeignKey(VirtualOrganisation)

    def __unicode__(self):
        return str(self.virtual_organisation)


class ActiveGridInteractions(models.Model):

    timestamp = models.DateTimeField(auto_now_add=True)
    tcp_address = models.CharField(max_length=255)
    grid_interaction = models.CharField(max_length=255)
    grid_id = models.CharField(max_length=255)
    robot_certificate = models.CharField(max_length=255)
    proxy_id = models.CharField(max_length=255)
    virtual_organisation = models.CharField(max_length=255)
    fqan = models.CharField(max_length=255)
    user_description = models.CharField(max_length=255)
    status = models.CharField(max_length=255)
    grid_ce = models.CharField(max_length=255)
    latitude = models.CharField(max_length=255)
    longitude = models.CharField(max_length=255)
    timestamp_endjob = models.CharField(max_length=255)

#     def __unicode__(self): #(return the relevant unicode string)
#         return self.guid

class CompletedGridInteractions(models.Model):

    guid = models.CharField(max_length=38)

    def __unicode__(self): #(return the relevant unicode string)
        return self.guid


admin.site.register(StoredFileMetadata)
admin.site.register(StorageElementBasePath)
admin.site.register(FilesSE)
admin.site.register(FileStorageParameters)
admin.site.register(FileVO)
admin.site.register(VirtualOrganisation)
admin.site.register(UserVO)
admin.site.register(CompletedGridInteractions)



