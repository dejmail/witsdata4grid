# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'StoredFileMetadata'
        db.create_table(u'filestorage_storedfilemetadata', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('description', self.gf('django.db.models.fields.TextField')(max_length=750)),
            ('subject', self.gf('django.db.models.fields.CharField')(max_length=25)),
            ('publisher', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('updated_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('format', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('identifier', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('source', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('language', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('relation', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('coverage', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('rights', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('creator', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
        ))
        db.send_create_signal(u'filestorage', ['StoredFileMetadata'])

        # Adding model 'FileStorageParameters'
        db.create_table(u'filestorage_filestorageparameters', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('filemetadata', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['filestorage.StoredFileMetadata'], unique=True)),
            ('filename', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'filestorage', ['FileStorageParameters'])

        # Adding model 'FilesSE'
        db.create_table(u'filestorage_filesse', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('file_info', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['filestorage.FileStorageParameters'])),
            ('se_info', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['filestorage.StorageElementBasePath'])),
        ))
        db.send_create_signal(u'filestorage', ['FilesSE'])

        # Adding model 'StorageElementBasePath'
        db.create_table(u'filestorage_storageelementbasepath', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('location', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('storage_element', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('base_path', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'filestorage', ['StorageElementBasePath'])

        # Adding model 'FileVO'
        db.create_table(u'filestorage_filevo', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('file_info', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['filestorage.FileStorageParameters'])),
            ('vo_info', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['filestorage.VirtualOrganisation'])),
        ))
        db.send_create_signal(u'filestorage', ['FileVO'])

        # Adding model 'VirtualOrganisation'
        db.create_table(u'filestorage_virtualorganisation', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('virtual_organisation', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'filestorage', ['VirtualOrganisation'])

        # Adding model 'UserVO'
        db.create_table(u'filestorage_uservo', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('username', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('virtual_organisation', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['filestorage.VirtualOrganisation'])),
        ))
        db.send_create_signal(u'filestorage', ['UserVO'])

        # Adding model 'ActiveGridInteractions'
        db.create_table(u'filestorage_activegridinteractions', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('timestamp', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('tcp_address', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('grid_interaction', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('grid_id', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('robot_certificate', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('proxy_id', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('virtual_organisation', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('fqan', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('user_description', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('grid_ce', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('latitude', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('longitude', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('timestamp_endjob', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'filestorage', ['ActiveGridInteractions'])

        # Adding model 'CompletedGridInteractions'
        db.create_table(u'filestorage_completedgridinteractions', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('guid', self.gf('django.db.models.fields.CharField')(max_length=38)),
        ))
        db.send_create_signal(u'filestorage', ['CompletedGridInteractions'])


    def backwards(self, orm):
        # Deleting model 'StoredFileMetadata'
        db.delete_table(u'filestorage_storedfilemetadata')

        # Deleting model 'FileStorageParameters'
        db.delete_table(u'filestorage_filestorageparameters')

        # Deleting model 'FilesSE'
        db.delete_table(u'filestorage_filesse')

        # Deleting model 'StorageElementBasePath'
        db.delete_table(u'filestorage_storageelementbasepath')

        # Deleting model 'FileVO'
        db.delete_table(u'filestorage_filevo')

        # Deleting model 'VirtualOrganisation'
        db.delete_table(u'filestorage_virtualorganisation')

        # Deleting model 'UserVO'
        db.delete_table(u'filestorage_uservo')

        # Deleting model 'ActiveGridInteractions'
        db.delete_table(u'filestorage_activegridinteractions')

        # Deleting model 'CompletedGridInteractions'
        db.delete_table(u'filestorage_completedgridinteractions')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'filestorage.activegridinteractions': {
            'Meta': {'object_name': 'ActiveGridInteractions'},
            'fqan': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'grid_ce': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'grid_id': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'grid_interaction': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'longitude': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'proxy_id': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'robot_certificate': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'tcp_address': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'timestamp_endjob': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'user_description': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'virtual_organisation': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'filestorage.completedgridinteractions': {
            'Meta': {'object_name': 'CompletedGridInteractions'},
            'guid': ('django.db.models.fields.CharField', [], {'max_length': '38'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'filestorage.filesse': {
            'Meta': {'object_name': 'FilesSE'},
            'file_info': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['filestorage.FileStorageParameters']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'se_info': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['filestorage.StorageElementBasePath']"})
        },
        u'filestorage.filestorageparameters': {
            'Meta': {'object_name': 'FileStorageParameters'},
            'file_vo': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['filestorage.VirtualOrganisation']", 'through': u"orm['filestorage.FileVO']", 'symmetrical': 'False'}),
            'filemetadata': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['filestorage.StoredFileMetadata']", 'unique': 'True'}),
            'filename': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'se_base_path': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['filestorage.StorageElementBasePath']", 'through': u"orm['filestorage.FilesSE']", 'symmetrical': 'False'})
        },
        u'filestorage.filevo': {
            'Meta': {'object_name': 'FileVO'},
            'file_info': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['filestorage.FileStorageParameters']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'vo_info': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['filestorage.VirtualOrganisation']"})
        },
        u'filestorage.storageelementbasepath': {
            'Meta': {'object_name': 'StorageElementBasePath'},
            'base_path': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'storage_element': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'filestorage.storedfilemetadata': {
            'Meta': {'ordering': "['title']", 'object_name': 'StoredFileMetadata'},
            'coverage': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'creator': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'description': ('django.db.models.fields.TextField', [], {'max_length': '750'}),
            'format': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identifier': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'language': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'publisher': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'relation': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'rights': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'source': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'filestorage.uservo': {
            'Meta': {'object_name': 'UserVO'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'username': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'virtual_organisation': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['filestorage.VirtualOrganisation']"})
        },
        u'filestorage.virtualorganisation': {
            'Meta': {'object_name': 'VirtualOrganisation'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user_id': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.User']", 'through': u"orm['filestorage.UserVO']", 'symmetrical': 'False'}),
            'virtual_organisation': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['filestorage']