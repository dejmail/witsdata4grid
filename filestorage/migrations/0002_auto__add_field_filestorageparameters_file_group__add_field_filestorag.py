# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'FileStorageParameters.file_group'
        db.add_column(u'filestorage_filestorageparameters', 'file_group',
                      self.gf('django.db.models.fields.related.OneToOneField')(default=3, to=orm['auth.Group'], unique=True),
                      keep_default=False)

        # Adding field 'FileStorageParameters.file_successfully_moved_to_se'
        db.add_column(u'filestorage_filestorageparameters', 'file_successfully_moved_to_se',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)

        # Adding field 'FileStorageParameters.file_successfully_uploaded_to_staging'
        db.add_column(u'filestorage_filestorageparameters', 'file_successfully_uploaded_to_staging',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'FileStorageParameters.file_group'
        db.delete_column(u'filestorage_filestorageparameters', 'file_group_id')

        # Deleting field 'FileStorageParameters.file_successfully_moved_to_se'
        db.delete_column(u'filestorage_filestorageparameters', 'file_successfully_moved_to_se')

        # Deleting field 'FileStorageParameters.file_successfully_uploaded_to_staging'
        db.delete_column(u'filestorage_filestorageparameters', 'file_successfully_uploaded_to_staging')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'filestorage.activegridinteractions': {
            'Meta': {'object_name': 'ActiveGridInteractions'},
            'fqan': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'grid_ce': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'grid_id': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'grid_interaction': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'latitude': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'longitude': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'proxy_id': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'robot_certificate': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'tcp_address': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'timestamp': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'timestamp_endjob': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'user_description': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'virtual_organisation': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'filestorage.completedgridinteractions': {
            'Meta': {'object_name': 'CompletedGridInteractions'},
            'guid': ('django.db.models.fields.CharField', [], {'max_length': '38'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'filestorage.filesse': {
            'Meta': {'object_name': 'FilesSE'},
            'file_info': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['filestorage.FileStorageParameters']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'se_info': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['filestorage.StorageElementBasePath']"})
        },
        u'filestorage.filestorageparameters': {
            'Meta': {'object_name': 'FileStorageParameters'},
            'file_group': ('django.db.models.fields.related.OneToOneField', [], {'default': '3', 'to': u"orm['auth.Group']", 'unique': 'True'}),
            'file_successfully_moved_to_se': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'file_successfully_uploaded_to_staging': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'file_vo': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['filestorage.VirtualOrganisation']", 'through': u"orm['filestorage.FileVO']", 'symmetrical': 'False'}),
            'filemetadata': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['filestorage.StoredFileMetadata']", 'unique': 'True'}),
            'filename': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'se_base_path': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['filestorage.StorageElementBasePath']", 'through': u"orm['filestorage.FilesSE']", 'symmetrical': 'False'})
        },
        u'filestorage.filevo': {
            'Meta': {'object_name': 'FileVO'},
            'file_info': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['filestorage.FileStorageParameters']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'vo_info': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['filestorage.VirtualOrganisation']"})
        },
        u'filestorage.storageelementbasepath': {
            'Meta': {'object_name': 'StorageElementBasePath'},
            'base_path': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'location': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'storage_element': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'filestorage.storedfilemetadata': {
            'Meta': {'ordering': "['title']", 'object_name': 'StoredFileMetadata'},
            'coverage': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'creator': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'description': ('django.db.models.fields.TextField', [], {'max_length': '750'}),
            'format': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'identifier': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'language': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'publisher': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'relation': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'rights': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'source': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'updated_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        },
        u'filestorage.uservo': {
            'Meta': {'object_name': 'UserVO'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'username': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'virtual_organisation': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['filestorage.VirtualOrganisation']"})
        },
        u'filestorage.virtualorganisation': {
            'Meta': {'object_name': 'VirtualOrganisation'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user_id': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.User']", 'through': u"orm['filestorage.UserVO']", 'symmetrical': 'False'}),
            'virtual_organisation': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['filestorage']