from django import forms

#from datetime import date
from models import StoredFileMetadata, FileStorageParameters, VirtualOrganisation, StorageElementBasePath
from django.contrib.auth.models import User, Group


import logging


class StoredFileMetadataForm(forms.ModelForm):
    #description = forms.CharField(widget=forms.Textarea(attrs={'size' : '40'}))

    class Meta:
        model = StoredFileMetadata
        exclude = ('created_at', 'updated_at', 'creator',)

class FileForm(forms.ModelForm):

    '''Excluded
    user = automatically pulled in from session
    filepath = not sure this is needed anymore
    project = covered by the Group choicefield
    virtual_organisation = covered by the Virtual_Organisation ChoiceField

    location = have not enabled cross ChoiceField functionality. I want the user to choose
    the institute and then the VO (VO being dependent on institute.
     '''
    class Meta:
        model = FileStorageParameters
        fields = ('filename', 'se_base_path', 'file_vo')
        exclude = ('filemetadata', 'file_successfully_moved_to_se', 'file_successfully_uploaded_to_staging')

    def __init__(self,  *args, **kwargs):

        self.location = kwargs.pop('location', None)
        self.project = kwargs.pop('project', None)
        self.virtual_organisation = kwargs.pop('virtual_organisation', None)

        super(FileForm, self).__init__(*args, **kwargs)

        self.fields['se_base_path'] = forms.ChoiceField(choices=self.location, required=False)
        self.fields['project'] = forms.ChoiceField(choices=self.project, required=False)
        self.fields['file_vo'] = forms.ChoiceField(choices=self.virtual_organisation, required=False)
        self.fields['filename'] = forms.CharField(max_length=255, required=False)

class SelectMetadata(forms.Form):

    choice_field = forms.ChoiceField(widget=forms.RadioSelect)

class UploadFileForm(forms.Form):
    #title = forms.CharField(max_length=50)
    file  = forms.FileField()

    #def clean_upload(self):
    #    upload = self.cleaned_data['upload']
    #    content_type = upload.content_type


