"""
WSGI config for witsdata4grid project.

This module contains the WSGI application used by Django's development server
and any production WSGI deployments. It should expose a module-level variable
named ``application``. Django's ``runserver`` and ``runfcgi`` commands discover
this application via the ``WSGI_APPLICATION`` setting.

Usually you will have the standard Django WSGI application here, but it also
might make sense to replace the whole Django WSGI application with a custom one
that later delegates to the Django one. For example, you could introduce WSGI
middleware here, or combine a Django application with an application of another
framework.

"""
import os
import sys
import site
import socket

if socket.gethostname().startswith('blueice'):
    DJANGO_HOST = 'development'
    sys.path.insert(0, '/opt/code_home/django/witsdata4grid/')
    site.addsitedir('/opt/code_home/django/witsdata4grid/env/lib/python2.6/site-packages/')
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings.local")
    
    #import settings

    #import django.core.management
    #django.core.management.setup_environ(settings)
    #utility = django.core.management.ManagementUtility()
    #command = utility.fetch_command('runserver')

    #command.validate()

    #import django.conf
    #import django.utils

    #django.utils.translation.activate(django.conf.settings.LANGUAGE_CODE)    

elif socket.gethostname().startswith('ray'):
    DJANGO_HOST = 'production'
    sys.path.insert(0, '/opt/python/witsdata4grid/')
    site.addsitedir('/opt/python/witsdata4grid/env/lib/python2.6/site-packages/')
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings.staging")
    
    #import settings

    #import django.core.management
    #django.core.management.setup_environ(settings)
    #utility = django.core.management.ManagementUtility()
    #command = utility.fetch_command('runserver')

    #command.validate()

    #import django.conf
    #import django.utils

    #django.utils.translation.activate(django.conf.settings.LANGUAGE_CODE)

# We defer to a DJANGO_SETTINGS_MODULE already in the environment. This breaks
# if running multiple sites in the same mod_wsgi process. To fix this, use
# mod_wsgi daemon mode with each site in its own daemon process, or use

#import djcelery
#djcelery.setup_loader()

import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()






