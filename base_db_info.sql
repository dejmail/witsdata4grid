--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO grid4data;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: grid4data
--

CREATE SEQUENCE auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO grid4data;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grid4data
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO grid4data;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: grid4data
--

CREATE SEQUENCE auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO grid4data;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grid4data
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO grid4data;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: grid4data
--

CREATE SEQUENCE auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO grid4data;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grid4data
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone NOT NULL,
    is_superuser boolean NOT NULL,
    username character varying(30) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(75) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO grid4data;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO grid4data;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: grid4data
--

CREATE SEQUENCE auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO grid4data;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grid4data
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: grid4data
--

CREATE SEQUENCE auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO grid4data;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grid4data
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO grid4data;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: grid4data
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO grid4data;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grid4data
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: celery_taskmeta; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE celery_taskmeta (
    id integer NOT NULL,
    task_id character varying(255) NOT NULL,
    status character varying(50) NOT NULL,
    result text,
    date_done timestamp with time zone NOT NULL,
    traceback text,
    hidden boolean NOT NULL,
    meta text
);


ALTER TABLE public.celery_taskmeta OWNER TO grid4data;

--
-- Name: celery_taskmeta_id_seq; Type: SEQUENCE; Schema: public; Owner: grid4data
--

CREATE SEQUENCE celery_taskmeta_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.celery_taskmeta_id_seq OWNER TO grid4data;

--
-- Name: celery_taskmeta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grid4data
--

ALTER SEQUENCE celery_taskmeta_id_seq OWNED BY celery_taskmeta.id;


--
-- Name: celery_tasksetmeta; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE celery_tasksetmeta (
    id integer NOT NULL,
    taskset_id character varying(255) NOT NULL,
    result text NOT NULL,
    date_done timestamp with time zone NOT NULL,
    hidden boolean NOT NULL
);


ALTER TABLE public.celery_tasksetmeta OWNER TO grid4data;

--
-- Name: celery_tasksetmeta_id_seq; Type: SEQUENCE; Schema: public; Owner: grid4data
--

CREATE SEQUENCE celery_tasksetmeta_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.celery_tasksetmeta_id_seq OWNER TO grid4data;

--
-- Name: celery_tasksetmeta_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grid4data
--

ALTER SEQUENCE celery_tasksetmeta_id_seq OWNED BY celery_tasksetmeta.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    user_id integer NOT NULL,
    content_type_id integer,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO grid4data;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: grid4data
--

CREATE SEQUENCE django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO grid4data;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grid4data
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    name character varying(100) NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO grid4data;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: grid4data
--

CREATE SEQUENCE django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO grid4data;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grid4data
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO grid4data;

--
-- Name: django_site; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE django_site (
    id integer NOT NULL,
    domain character varying(100) NOT NULL,
    name character varying(50) NOT NULL
);


ALTER TABLE public.django_site OWNER TO grid4data;

--
-- Name: django_site_id_seq; Type: SEQUENCE; Schema: public; Owner: grid4data
--

CREATE SEQUENCE django_site_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_site_id_seq OWNER TO grid4data;

--
-- Name: django_site_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grid4data
--

ALTER SEQUENCE django_site_id_seq OWNED BY django_site.id;


--
-- Name: djcelery_crontabschedule; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE djcelery_crontabschedule (
    id integer NOT NULL,
    minute character varying(64) NOT NULL,
    hour character varying(64) NOT NULL,
    day_of_week character varying(64) NOT NULL,
    day_of_month character varying(64) NOT NULL,
    month_of_year character varying(64) NOT NULL
);


ALTER TABLE public.djcelery_crontabschedule OWNER TO grid4data;

--
-- Name: djcelery_crontabschedule_id_seq; Type: SEQUENCE; Schema: public; Owner: grid4data
--

CREATE SEQUENCE djcelery_crontabschedule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.djcelery_crontabschedule_id_seq OWNER TO grid4data;

--
-- Name: djcelery_crontabschedule_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grid4data
--

ALTER SEQUENCE djcelery_crontabschedule_id_seq OWNED BY djcelery_crontabschedule.id;


--
-- Name: djcelery_intervalschedule; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE djcelery_intervalschedule (
    id integer NOT NULL,
    every integer NOT NULL,
    period character varying(24) NOT NULL
);


ALTER TABLE public.djcelery_intervalschedule OWNER TO grid4data;

--
-- Name: djcelery_intervalschedule_id_seq; Type: SEQUENCE; Schema: public; Owner: grid4data
--

CREATE SEQUENCE djcelery_intervalschedule_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.djcelery_intervalschedule_id_seq OWNER TO grid4data;

--
-- Name: djcelery_intervalschedule_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grid4data
--

ALTER SEQUENCE djcelery_intervalschedule_id_seq OWNED BY djcelery_intervalschedule.id;


--
-- Name: djcelery_periodictask; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE djcelery_periodictask (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    task character varying(200) NOT NULL,
    interval_id integer,
    crontab_id integer,
    args text NOT NULL,
    kwargs text NOT NULL,
    queue character varying(200),
    exchange character varying(200),
    routing_key character varying(200),
    expires timestamp with time zone,
    enabled boolean NOT NULL,
    last_run_at timestamp with time zone,
    total_run_count integer NOT NULL,
    date_changed timestamp with time zone NOT NULL,
    description text NOT NULL,
    CONSTRAINT djcelery_periodictask_total_run_count_check CHECK ((total_run_count >= 0))
);


ALTER TABLE public.djcelery_periodictask OWNER TO grid4data;

--
-- Name: djcelery_periodictask_id_seq; Type: SEQUENCE; Schema: public; Owner: grid4data
--

CREATE SEQUENCE djcelery_periodictask_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.djcelery_periodictask_id_seq OWNER TO grid4data;

--
-- Name: djcelery_periodictask_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grid4data
--

ALTER SEQUENCE djcelery_periodictask_id_seq OWNED BY djcelery_periodictask.id;


--
-- Name: djcelery_periodictasks; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE djcelery_periodictasks (
    ident smallint NOT NULL,
    last_update timestamp with time zone NOT NULL
);


ALTER TABLE public.djcelery_periodictasks OWNER TO grid4data;

--
-- Name: djcelery_taskstate; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE djcelery_taskstate (
    id integer NOT NULL,
    state character varying(64) NOT NULL,
    task_id character varying(36) NOT NULL,
    name character varying(200),
    tstamp timestamp with time zone NOT NULL,
    args text,
    kwargs text,
    eta timestamp with time zone,
    expires timestamp with time zone,
    result text,
    traceback text,
    runtime double precision,
    retries integer NOT NULL,
    worker_id integer,
    hidden boolean NOT NULL
);


ALTER TABLE public.djcelery_taskstate OWNER TO grid4data;

--
-- Name: djcelery_taskstate_id_seq; Type: SEQUENCE; Schema: public; Owner: grid4data
--

CREATE SEQUENCE djcelery_taskstate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.djcelery_taskstate_id_seq OWNER TO grid4data;

--
-- Name: djcelery_taskstate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grid4data
--

ALTER SEQUENCE djcelery_taskstate_id_seq OWNED BY djcelery_taskstate.id;


--
-- Name: djcelery_workerstate; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE djcelery_workerstate (
    id integer NOT NULL,
    hostname character varying(255) NOT NULL,
    last_heartbeat timestamp with time zone
);


ALTER TABLE public.djcelery_workerstate OWNER TO grid4data;

--
-- Name: djcelery_workerstate_id_seq; Type: SEQUENCE; Schema: public; Owner: grid4data
--

CREATE SEQUENCE djcelery_workerstate_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.djcelery_workerstate_id_seq OWNER TO grid4data;

--
-- Name: djcelery_workerstate_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grid4data
--

ALTER SEQUENCE djcelery_workerstate_id_seq OWNED BY djcelery_workerstate.id;


--
-- Name: filestorage_activegridinteractions; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE filestorage_activegridinteractions (
    id integer NOT NULL,
    "timestamp" timestamp with time zone NOT NULL,
    tcp_address character varying(255) NOT NULL,
    grid_interaction character varying(255) NOT NULL,
    grid_id character varying(255) NOT NULL,
    robot_certificate character varying(255) NOT NULL,
    proxy_id character varying(255) NOT NULL,
    virtual_organisation character varying(255) NOT NULL,
    fqan character varying(255) NOT NULL,
    user_description character varying(255) NOT NULL,
    status character varying(255) NOT NULL,
    grid_ce character varying(255) NOT NULL,
    latitude character varying(255) NOT NULL,
    longitude character varying(255) NOT NULL,
    timestamp_endjob character varying(255) NOT NULL
);


ALTER TABLE public.filestorage_activegridinteractions OWNER TO grid4data;

--
-- Name: filestorage_activegridinteractions_id_seq; Type: SEQUENCE; Schema: public; Owner: grid4data
--

CREATE SEQUENCE filestorage_activegridinteractions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filestorage_activegridinteractions_id_seq OWNER TO grid4data;

--
-- Name: filestorage_activegridinteractions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grid4data
--

ALTER SEQUENCE filestorage_activegridinteractions_id_seq OWNED BY filestorage_activegridinteractions.id;


--
-- Name: filestorage_completedgridinteractions; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE filestorage_completedgridinteractions (
    id integer NOT NULL,
    guid character varying(38) NOT NULL
);


ALTER TABLE public.filestorage_completedgridinteractions OWNER TO grid4data;

--
-- Name: filestorage_completedgridinteractions_id_seq; Type: SEQUENCE; Schema: public; Owner: grid4data
--

CREATE SEQUENCE filestorage_completedgridinteractions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filestorage_completedgridinteractions_id_seq OWNER TO grid4data;

--
-- Name: filestorage_completedgridinteractions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grid4data
--

ALTER SEQUENCE filestorage_completedgridinteractions_id_seq OWNED BY filestorage_completedgridinteractions.id;


--
-- Name: filestorage_filesse; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE filestorage_filesse (
    id integer NOT NULL,
    file_info_id integer NOT NULL,
    se_info_id integer NOT NULL
);


ALTER TABLE public.filestorage_filesse OWNER TO grid4data;

--
-- Name: filestorage_filesse_id_seq; Type: SEQUENCE; Schema: public; Owner: grid4data
--

CREATE SEQUENCE filestorage_filesse_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filestorage_filesse_id_seq OWNER TO grid4data;

--
-- Name: filestorage_filesse_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grid4data
--

ALTER SEQUENCE filestorage_filesse_id_seq OWNED BY filestorage_filesse.id;


--
-- Name: filestorage_filestorageparameters; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE filestorage_filestorageparameters (
    id integer NOT NULL,
    filemetadata_id integer NOT NULL,
    filename character varying(255) NOT NULL,
    file_group_id integer NOT NULL,
    file_successfully_moved_to_se boolean NOT NULL,
    file_successfully_uploaded_to_staging boolean NOT NULL
);


ALTER TABLE public.filestorage_filestorageparameters OWNER TO grid4data;

--
-- Name: filestorage_filestorageparameters_id_seq; Type: SEQUENCE; Schema: public; Owner: grid4data
--

CREATE SEQUENCE filestorage_filestorageparameters_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filestorage_filestorageparameters_id_seq OWNER TO grid4data;

--
-- Name: filestorage_filestorageparameters_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grid4data
--

ALTER SEQUENCE filestorage_filestorageparameters_id_seq OWNED BY filestorage_filestorageparameters.id;


--
-- Name: filestorage_filevo; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE filestorage_filevo (
    id integer NOT NULL,
    file_info_id integer NOT NULL,
    vo_info_id integer NOT NULL
);


ALTER TABLE public.filestorage_filevo OWNER TO grid4data;

--
-- Name: filestorage_filevo_id_seq; Type: SEQUENCE; Schema: public; Owner: grid4data
--

CREATE SEQUENCE filestorage_filevo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filestorage_filevo_id_seq OWNER TO grid4data;

--
-- Name: filestorage_filevo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grid4data
--

ALTER SEQUENCE filestorage_filevo_id_seq OWNED BY filestorage_filevo.id;


--
-- Name: filestorage_storageelementbasepath; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE filestorage_storageelementbasepath (
    id integer NOT NULL,
    location character varying(50) NOT NULL,
    storage_element character varying(255) NOT NULL,
    base_path character varying(255) NOT NULL
);


ALTER TABLE public.filestorage_storageelementbasepath OWNER TO grid4data;

--
-- Name: filestorage_storageelementbasepath_id_seq; Type: SEQUENCE; Schema: public; Owner: grid4data
--

CREATE SEQUENCE filestorage_storageelementbasepath_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filestorage_storageelementbasepath_id_seq OWNER TO grid4data;

--
-- Name: filestorage_storageelementbasepath_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grid4data
--

ALTER SEQUENCE filestorage_storageelementbasepath_id_seq OWNED BY filestorage_storageelementbasepath.id;


--
-- Name: filestorage_storedfilemetadata; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE filestorage_storedfilemetadata (
    id integer NOT NULL,
    title character varying(150) NOT NULL,
    description text NOT NULL,
    subject character varying(25) NOT NULL,
    publisher character varying(20) NOT NULL,
    created_at timestamp with time zone NOT NULL,
    updated_at timestamp with time zone NOT NULL,
    type character varying(20) NOT NULL,
    format character varying(20) NOT NULL,
    identifier character varying(20) NOT NULL,
    source character varying(20) NOT NULL,
    language character varying(10) NOT NULL,
    relation character varying(20) NOT NULL,
    coverage character varying(20) NOT NULL,
    rights character varying(20) NOT NULL,
    creator_id integer NOT NULL
);


ALTER TABLE public.filestorage_storedfilemetadata OWNER TO grid4data;

--
-- Name: filestorage_storedfilemetadata_id_seq; Type: SEQUENCE; Schema: public; Owner: grid4data
--

CREATE SEQUENCE filestorage_storedfilemetadata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filestorage_storedfilemetadata_id_seq OWNER TO grid4data;

--
-- Name: filestorage_storedfilemetadata_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grid4data
--

ALTER SEQUENCE filestorage_storedfilemetadata_id_seq OWNED BY filestorage_storedfilemetadata.id;


--
-- Name: filestorage_uservo; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE filestorage_uservo (
    id integer NOT NULL,
    username_id integer NOT NULL,
    virtual_organisation_id integer NOT NULL
);


ALTER TABLE public.filestorage_uservo OWNER TO grid4data;

--
-- Name: filestorage_uservo_id_seq; Type: SEQUENCE; Schema: public; Owner: grid4data
--

CREATE SEQUENCE filestorage_uservo_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filestorage_uservo_id_seq OWNER TO grid4data;

--
-- Name: filestorage_uservo_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grid4data
--

ALTER SEQUENCE filestorage_uservo_id_seq OWNED BY filestorage_uservo.id;


--
-- Name: filestorage_virtualorganisation; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE filestorage_virtualorganisation (
    id integer NOT NULL,
    virtual_organisation character varying(255) NOT NULL
);


ALTER TABLE public.filestorage_virtualorganisation OWNER TO grid4data;

--
-- Name: filestorage_virtualorganisation_id_seq; Type: SEQUENCE; Schema: public; Owner: grid4data
--

CREATE SEQUENCE filestorage_virtualorganisation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.filestorage_virtualorganisation_id_seq OWNER TO grid4data;

--
-- Name: filestorage_virtualorganisation_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grid4data
--

ALTER SEQUENCE filestorage_virtualorganisation_id_seq OWNED BY filestorage_virtualorganisation.id;


--
-- Name: lfcinteract_newdata; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE lfcinteract_newdata (
    id integer NOT NULL,
    title character varying(150) NOT NULL,
    description text NOT NULL,
    creator character varying(50) NOT NULL,
    subject character varying(25) NOT NULL,
    publisher character varying(20) NOT NULL,
    contributor character varying(20) NOT NULL,
    date timestamp with time zone NOT NULL,
    tpe character varying(20) NOT NULL,
    frmat character varying(20) NOT NULL,
    identifier character varying(20) NOT NULL,
    source character varying(20) NOT NULL,
    language character varying(10) NOT NULL,
    relation character varying(20) NOT NULL,
    coverage character varying(20) NOT NULL,
    rights character varying(20) NOT NULL
);


ALTER TABLE public.lfcinteract_newdata OWNER TO grid4data;

--
-- Name: lfcinteract_newdata_id_seq; Type: SEQUENCE; Schema: public; Owner: grid4data
--

CREATE SEQUENCE lfcinteract_newdata_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lfcinteract_newdata_id_seq OWNER TO grid4data;

--
-- Name: lfcinteract_newdata_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grid4data
--

ALTER SEQUENCE lfcinteract_newdata_id_seq OWNED BY lfcinteract_newdata.id;


--
-- Name: proxy_robotproxydetails; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE proxy_robotproxydetails (
    id integer NOT NULL,
    server_name character varying(16) NOT NULL,
    priority integer NOT NULL,
    url character varying(200) NOT NULL,
    md5 character varying(32) NOT NULL,
    voms_id integer NOT NULL,
    proxy_renewal boolean NOT NULL,
    disable_voms_proxy boolean NOT NULL,
    rfc_proxy boolean NOT NULL
);


ALTER TABLE public.proxy_robotproxydetails OWNER TO grid4data;

--
-- Name: proxy_robotproxydetails_id_seq; Type: SEQUENCE; Schema: public; Owner: grid4data
--

CREATE SEQUENCE proxy_robotproxydetails_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.proxy_robotproxydetails_id_seq OWNER TO grid4data;

--
-- Name: proxy_robotproxydetails_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grid4data
--

ALTER SEQUENCE proxy_robotproxydetails_id_seq OWNED BY proxy_robotproxydetails.id;


--
-- Name: south_migrationhistory; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE south_migrationhistory (
    id integer NOT NULL,
    app_name character varying(255) NOT NULL,
    migration character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.south_migrationhistory OWNER TO grid4data;

--
-- Name: south_migrationhistory_id_seq; Type: SEQUENCE; Schema: public; Owner: grid4data
--

CREATE SEQUENCE south_migrationhistory_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.south_migrationhistory_id_seq OWNER TO grid4data;

--
-- Name: south_migrationhistory_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grid4data
--

ALTER SEQUENCE south_migrationhistory_id_seq OWNED BY south_migrationhistory.id;


--
-- Name: ssl_authenticate_userprofile; Type: TABLE; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE TABLE ssl_authenticate_userprofile (
    id integer NOT NULL,
    user_id integer NOT NULL,
    institution character varying(75) NOT NULL,
    groups text NOT NULL
);


ALTER TABLE public.ssl_authenticate_userprofile OWNER TO grid4data;

--
-- Name: ssl_authenticate_userprofile_id_seq; Type: SEQUENCE; Schema: public; Owner: grid4data
--

CREATE SEQUENCE ssl_authenticate_userprofile_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ssl_authenticate_userprofile_id_seq OWNER TO grid4data;

--
-- Name: ssl_authenticate_userprofile_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: grid4data
--

ALTER SEQUENCE ssl_authenticate_userprofile_id_seq OWNED BY ssl_authenticate_userprofile.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY celery_taskmeta ALTER COLUMN id SET DEFAULT nextval('celery_taskmeta_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY celery_tasksetmeta ALTER COLUMN id SET DEFAULT nextval('celery_tasksetmeta_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY django_site ALTER COLUMN id SET DEFAULT nextval('django_site_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY djcelery_crontabschedule ALTER COLUMN id SET DEFAULT nextval('djcelery_crontabschedule_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY djcelery_intervalschedule ALTER COLUMN id SET DEFAULT nextval('djcelery_intervalschedule_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY djcelery_periodictask ALTER COLUMN id SET DEFAULT nextval('djcelery_periodictask_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY djcelery_taskstate ALTER COLUMN id SET DEFAULT nextval('djcelery_taskstate_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY djcelery_workerstate ALTER COLUMN id SET DEFAULT nextval('djcelery_workerstate_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY filestorage_activegridinteractions ALTER COLUMN id SET DEFAULT nextval('filestorage_activegridinteractions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY filestorage_completedgridinteractions ALTER COLUMN id SET DEFAULT nextval('filestorage_completedgridinteractions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY filestorage_filesse ALTER COLUMN id SET DEFAULT nextval('filestorage_filesse_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY filestorage_filestorageparameters ALTER COLUMN id SET DEFAULT nextval('filestorage_filestorageparameters_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY filestorage_filevo ALTER COLUMN id SET DEFAULT nextval('filestorage_filevo_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY filestorage_storageelementbasepath ALTER COLUMN id SET DEFAULT nextval('filestorage_storageelementbasepath_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY filestorage_storedfilemetadata ALTER COLUMN id SET DEFAULT nextval('filestorage_storedfilemetadata_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY filestorage_uservo ALTER COLUMN id SET DEFAULT nextval('filestorage_uservo_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY filestorage_virtualorganisation ALTER COLUMN id SET DEFAULT nextval('filestorage_virtualorganisation_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY lfcinteract_newdata ALTER COLUMN id SET DEFAULT nextval('lfcinteract_newdata_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY proxy_robotproxydetails ALTER COLUMN id SET DEFAULT nextval('proxy_robotproxydetails_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY south_migrationhistory ALTER COLUMN id SET DEFAULT nextval('south_migrationhistory_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY ssl_authenticate_userprofile ALTER COLUMN id SET DEFAULT nextval('ssl_authenticate_userprofile_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY auth_group (id, name) FROM stdin;
1	h3abionet
2	data-test
3	admin
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grid4data
--

SELECT pg_catalog.setval('auth_group_id_seq', 3, true);


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
1	1	25
2	1	43
3	1	46
4	1	22
5	2	1
6	2	2
7	2	3
8	2	4
9	2	5
10	2	6
11	2	7
12	2	8
13	2	9
14	2	10
15	2	11
16	2	12
17	2	13
18	2	14
19	2	15
20	2	16
21	2	17
22	2	18
23	2	19
24	2	20
25	2	21
26	2	22
27	2	23
28	2	24
29	2	25
30	2	26
31	2	27
32	2	28
33	2	29
34	2	30
35	2	31
36	2	32
37	2	33
38	2	34
39	2	35
40	2	36
41	2	37
42	2	38
43	2	39
44	2	40
45	2	41
46	2	42
47	2	43
48	2	44
49	2	45
50	2	46
51	2	47
52	2	48
53	2	49
54	2	50
55	2	51
56	2	52
57	2	53
58	2	54
59	2	55
60	2	56
61	2	57
62	2	58
63	2	59
64	2	60
65	2	61
66	2	62
67	2	63
68	2	64
69	2	65
70	2	66
71	2	67
72	2	68
73	2	69
74	2	70
75	2	71
76	2	72
77	2	73
78	2	74
79	2	75
80	2	76
81	2	77
82	2	78
83	2	79
84	2	80
85	2	81
86	2	82
87	2	83
88	2	84
89	3	1
90	3	2
91	3	3
92	3	4
93	3	5
94	3	6
95	3	7
96	3	8
97	3	9
98	3	10
99	3	11
100	3	12
101	3	13
102	3	14
103	3	15
104	3	16
105	3	17
106	3	18
107	3	19
108	3	20
109	3	21
110	3	22
111	3	23
112	3	24
113	3	25
114	3	26
115	3	27
116	3	28
117	3	29
118	3	30
119	3	31
120	3	32
121	3	33
122	3	34
123	3	35
124	3	36
125	3	37
126	3	38
127	3	39
128	3	40
129	3	41
130	3	42
131	3	43
132	3	44
133	3	45
134	3	46
135	3	47
136	3	48
137	3	49
138	3	50
139	3	51
140	3	52
141	3	53
142	3	54
143	3	55
144	3	56
145	3	57
146	3	58
147	3	59
148	3	60
149	3	61
150	3	62
151	3	63
152	3	64
153	3	65
154	3	66
155	3	67
156	3	68
157	3	69
158	3	70
159	3	71
160	3	72
161	3	73
162	3	74
163	3	75
164	3	76
165	3	77
166	3	78
167	3	79
168	3	80
169	3	81
170	3	82
171	3	83
172	3	84
\.


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grid4data
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 172, true);


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add permission	1	add_permission
2	Can change permission	1	change_permission
3	Can delete permission	1	delete_permission
4	Can add group	2	add_group
5	Can change group	2	change_group
6	Can delete group	2	delete_group
7	Can add user	3	add_user
8	Can change user	3	change_user
9	Can delete user	3	delete_user
10	Can add content type	4	add_contenttype
11	Can change content type	4	change_contenttype
12	Can delete content type	4	delete_contenttype
13	Can add session	5	add_session
14	Can change session	5	change_session
15	Can delete session	5	delete_session
16	Can add site	6	add_site
17	Can change site	6	change_site
18	Can delete site	6	delete_site
19	Can add log entry	7	add_logentry
20	Can change log entry	7	change_logentry
21	Can delete log entry	7	delete_logentry
22	Can add stored file metadata	8	add_storedfilemetadata
23	Can change stored file metadata	8	change_storedfilemetadata
24	Can delete stored file metadata	8	delete_storedfilemetadata
25	Can add file storage parameters	9	add_filestorageparameters
26	Can change file storage parameters	9	change_filestorageparameters
27	Can delete file storage parameters	9	delete_filestorageparameters
28	Can add files se	10	add_filesse
29	Can change files se	10	change_filesse
30	Can delete files se	10	delete_filesse
31	Can add storage element base path	11	add_storageelementbasepath
32	Can change storage element base path	11	change_storageelementbasepath
33	Can delete storage element base path	11	delete_storageelementbasepath
34	Can add file vo	12	add_filevo
35	Can change file vo	12	change_filevo
36	Can delete file vo	12	delete_filevo
37	Can add virtual organisation	13	add_virtualorganisation
38	Can change virtual organisation	13	change_virtualorganisation
39	Can delete virtual organisation	13	delete_virtualorganisation
40	Can add user vo	14	add_uservo
41	Can change user vo	14	change_uservo
42	Can delete user vo	14	delete_uservo
43	Can add active grid interactions	15	add_activegridinteractions
44	Can change active grid interactions	15	change_activegridinteractions
45	Can delete active grid interactions	15	delete_activegridinteractions
46	Can add completed grid interactions	16	add_completedgridinteractions
47	Can change completed grid interactions	16	change_completedgridinteractions
48	Can delete completed grid interactions	16	delete_completedgridinteractions
49	Can add new data	17	add_newdata
50	Can change new data	17	change_newdata
51	Can delete new data	17	delete_newdata
52	Can add robot proxy details	18	add_robotproxydetails
53	Can change robot proxy details	18	change_robotproxydetails
54	Can delete robot proxy details	18	delete_robotproxydetails
55	Can add user profile	19	add_userprofile
56	Can change user profile	19	change_userprofile
57	Can delete user profile	19	delete_userprofile
58	Can add migration history	20	add_migrationhistory
59	Can change migration history	20	change_migrationhistory
60	Can delete migration history	20	delete_migrationhistory
61	Can add task state	21	add_taskmeta
62	Can change task state	21	change_taskmeta
63	Can delete task state	21	delete_taskmeta
64	Can add saved group result	22	add_tasksetmeta
65	Can change saved group result	22	change_tasksetmeta
66	Can delete saved group result	22	delete_tasksetmeta
67	Can add interval	23	add_intervalschedule
68	Can change interval	23	change_intervalschedule
69	Can delete interval	23	delete_intervalschedule
70	Can add crontab	24	add_crontabschedule
71	Can change crontab	24	change_crontabschedule
72	Can delete crontab	24	delete_crontabschedule
73	Can add periodic tasks	25	add_periodictasks
74	Can change periodic tasks	25	change_periodictasks
75	Can delete periodic tasks	25	delete_periodictasks
76	Can add periodic task	26	add_periodictask
77	Can change periodic task	26	change_periodictask
78	Can delete periodic task	26	delete_periodictask
79	Can add worker	27	add_workerstate
80	Can change worker	27	change_workerstate
81	Can delete worker	27	delete_workerstate
82	Can add task	28	add_taskstate
83	Can change task	28	change_taskstate
84	Can delete task	28	delete_taskstate
\.


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grid4data
--

SELECT pg_catalog.setval('auth_permission_id_seq', 84, true);


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
1	pbkdf2_sha256$12000$ONalLlG5Zd68$mvHGYcYuPGTTp84dnr47qQM5i/VqV2kK9M9lgs+thWY=	2014-10-29 09:31:36+01	t	liamthompson	Liam	Thompson	dejmail@gmail.com	t	t	2014-10-29 09:27:54+01
2	pbkdf2_sha256$12000$vP2DeCOejire$cLhEjTRb71wQzgkU/2GSz3QkzO9X53tOR4EjuimPI28=	2014-10-29 09:39:59+01	t	liamjedthompson	Liam	Thompson	dejmail@gmail.com	t	t	2014-10-29 09:39:59+01
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY auth_user_groups (id, user_id, group_id) FROM stdin;
1	1	1
2	1	2
3	1	3
4	2	1
5	2	2
6	2	3
\.


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grid4data
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 6, true);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grid4data
--

SELECT pg_catalog.setval('auth_user_id_seq', 2, true);


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
1	1	1
2	1	2
3	1	3
4	1	4
5	1	5
6	1	6
7	1	7
8	1	8
9	1	9
10	1	10
11	1	11
12	1	12
13	1	13
14	1	14
15	1	15
16	1	16
17	1	17
18	1	18
19	1	19
20	1	20
21	1	21
22	1	22
23	1	23
24	1	24
25	1	25
26	1	26
27	1	27
28	1	28
29	1	29
30	1	30
31	1	31
32	1	32
33	1	33
34	1	34
35	1	35
36	1	36
37	1	37
38	1	38
39	1	39
40	1	40
41	1	41
42	1	42
43	1	43
44	1	44
45	1	45
46	1	46
47	1	47
48	1	48
49	1	49
50	1	50
51	1	51
52	1	52
53	1	53
54	1	54
55	1	55
56	1	56
57	1	57
58	1	58
59	1	59
60	1	60
61	1	61
62	1	62
63	1	63
64	1	64
65	1	65
66	1	66
67	1	67
68	1	68
69	1	69
70	1	70
71	1	71
72	1	72
73	1	73
74	1	74
75	1	75
76	1	76
77	1	77
78	1	78
79	1	79
80	1	80
81	1	81
82	1	82
83	1	83
84	1	84
85	2	1
86	2	2
87	2	3
88	2	4
89	2	5
90	2	6
91	2	7
92	2	8
93	2	9
94	2	10
95	2	11
96	2	12
97	2	13
98	2	14
99	2	15
100	2	16
101	2	17
102	2	18
103	2	19
104	2	20
105	2	21
106	2	22
107	2	23
108	2	24
109	2	25
110	2	26
111	2	27
112	2	28
113	2	29
114	2	30
115	2	31
116	2	32
117	2	33
118	2	34
119	2	35
120	2	36
121	2	37
122	2	38
123	2	39
124	2	40
125	2	41
126	2	42
127	2	43
128	2	44
129	2	45
130	2	46
131	2	47
132	2	48
133	2	49
134	2	50
135	2	51
136	2	52
137	2	53
138	2	54
139	2	55
140	2	56
141	2	57
142	2	58
143	2	59
144	2	60
145	2	61
146	2	62
147	2	63
148	2	64
149	2	65
150	2	66
151	2	67
152	2	68
153	2	69
154	2	70
155	2	71
156	2	72
157	2	73
158	2	74
159	2	75
160	2	76
161	2	77
162	2	78
163	2	79
164	2	80
165	2	81
166	2	82
167	2	83
168	2	84
\.


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grid4data
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 168, true);


--
-- Data for Name: celery_taskmeta; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY celery_taskmeta (id, task_id, status, result, date_done, traceback, hidden, meta) FROM stdin;
\.


--
-- Name: celery_taskmeta_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grid4data
--

SELECT pg_catalog.setval('celery_taskmeta_id_seq', 1, false);


--
-- Data for Name: celery_tasksetmeta; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY celery_tasksetmeta (id, taskset_id, result, date_done, hidden) FROM stdin;
\.


--
-- Name: celery_tasksetmeta_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grid4data
--

SELECT pg_catalog.setval('celery_tasksetmeta_id_seq', 1, false);


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY django_admin_log (id, action_time, user_id, content_type_id, object_id, object_repr, action_flag, change_message) FROM stdin;
1	2014-10-29 09:32:52.481171+01	1	13	1	sagrid	1	
2	2014-10-29 09:32:57.155417+01	1	13	2	awigen	1	
3	2014-10-29 09:33:00.776341+01	1	13	3	cern	1	
4	2014-10-29 09:34:37.451792+01	1	2	1	h3abionet	1	
5	2014-10-29 09:34:50.691766+01	1	2	2	data-test	1	
6	2014-10-29 09:35:03.441829+01	1	2	3	admin	1	
7	2014-10-29 09:35:17.416696+01	1	14	1	sagrid	1	
8	2014-10-29 09:35:22.875063+01	1	14	2	awigen	1	
9	2014-10-29 09:35:51.150343+01	1	11	1	se.core.wits.ac.za	1	
10	2014-10-29 09:37:31.693893+01	1	3	1	liamthompson	2	Changed first_name, last_name, groups and user_permissions.
11	2014-10-29 09:39:14.611621+01	1	11	1	se.core.wits.ac.za	2	Changed base_path.
12	2014-10-29 09:39:59.676811+01	1	3	2	liamjedthompson	1	
13	2014-10-29 09:40:21.916216+01	1	3	2	liamjedthompson	2	Changed first_name, last_name, email, is_staff, is_superuser, groups and user_permissions.
\.


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grid4data
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 13, true);


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY django_content_type (id, name, app_label, model) FROM stdin;
1	permission	auth	permission
2	group	auth	group
3	user	auth	user
4	content type	contenttypes	contenttype
5	session	sessions	session
6	site	sites	site
7	log entry	admin	logentry
8	stored file metadata	filestorage	storedfilemetadata
9	file storage parameters	filestorage	filestorageparameters
10	files se	filestorage	filesse
11	storage element base path	filestorage	storageelementbasepath
12	file vo	filestorage	filevo
13	virtual organisation	filestorage	virtualorganisation
14	user vo	filestorage	uservo
15	active grid interactions	filestorage	activegridinteractions
16	completed grid interactions	filestorage	completedgridinteractions
17	new data	lfcinteract	newdata
18	robot proxy details	proxy	robotproxydetails
19	user profile	ssl_authenticate	userprofile
20	migration history	south	migrationhistory
21	task state	djcelery	taskmeta
22	saved group result	djcelery	tasksetmeta
23	interval	djcelery	intervalschedule
24	crontab	djcelery	crontabschedule
25	periodic tasks	djcelery	periodictasks
26	periodic task	djcelery	periodictask
27	worker	djcelery	workerstate
28	task	djcelery	taskstate
\.


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grid4data
--

SELECT pg_catalog.setval('django_content_type_id_seq', 28, true);


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
j2qddz3a7ykgl9ruly76arcy2vt1rxwa	NzE2M2VmNTZjNGNlODg2NWYyNWNjNjQyODMzN2VjOTI5OWJlNjU4Mzp7Im1ldGFwayI6MywiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2lkIjoxLCJ1cGxvYWR1cmwiOiJodHRwczovL3NlLmNvcmUud2l0cy5hYy56YWRwbS9jb3JlLndpdHMuYWMuemEvaG9tZS9zYWdyaWQvd2l0c2RhdGE0Z3JpZC9oM2FiaW9uZXQvdGVzdGZpbGUtMjkxMC0wOTM3LnRneiJ9	2014-11-12 09:37:53.023118+01
\.


--
-- Data for Name: django_site; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY django_site (id, domain, name) FROM stdin;
1	example.com	example.com
\.


--
-- Name: django_site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grid4data
--

SELECT pg_catalog.setval('django_site_id_seq', 1, true);


--
-- Data for Name: djcelery_crontabschedule; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY djcelery_crontabschedule (id, minute, hour, day_of_week, day_of_month, month_of_year) FROM stdin;
\.


--
-- Name: djcelery_crontabschedule_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grid4data
--

SELECT pg_catalog.setval('djcelery_crontabschedule_id_seq', 1, false);


--
-- Data for Name: djcelery_intervalschedule; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY djcelery_intervalschedule (id, every, period) FROM stdin;
\.


--
-- Name: djcelery_intervalschedule_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grid4data
--

SELECT pg_catalog.setval('djcelery_intervalschedule_id_seq', 1, false);


--
-- Data for Name: djcelery_periodictask; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY djcelery_periodictask (id, name, task, interval_id, crontab_id, args, kwargs, queue, exchange, routing_key, expires, enabled, last_run_at, total_run_count, date_changed, description) FROM stdin;
\.


--
-- Name: djcelery_periodictask_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grid4data
--

SELECT pg_catalog.setval('djcelery_periodictask_id_seq', 1, false);


--
-- Data for Name: djcelery_periodictasks; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY djcelery_periodictasks (ident, last_update) FROM stdin;
\.


--
-- Data for Name: djcelery_taskstate; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY djcelery_taskstate (id, state, task_id, name, tstamp, args, kwargs, eta, expires, result, traceback, runtime, retries, worker_id, hidden) FROM stdin;
\.


--
-- Name: djcelery_taskstate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grid4data
--

SELECT pg_catalog.setval('djcelery_taskstate_id_seq', 1, false);


--
-- Data for Name: djcelery_workerstate; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY djcelery_workerstate (id, hostname, last_heartbeat) FROM stdin;
\.


--
-- Name: djcelery_workerstate_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grid4data
--

SELECT pg_catalog.setval('djcelery_workerstate_id_seq', 1, false);


--
-- Data for Name: filestorage_activegridinteractions; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY filestorage_activegridinteractions (id, "timestamp", tcp_address, grid_interaction, grid_id, robot_certificate, proxy_id, virtual_organisation, fqan, user_description, status, grid_ce, latitude, longitude, timestamp_endjob) FROM stdin;
\.


--
-- Name: filestorage_activegridinteractions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grid4data
--

SELECT pg_catalog.setval('filestorage_activegridinteractions_id_seq', 1, false);


--
-- Data for Name: filestorage_completedgridinteractions; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY filestorage_completedgridinteractions (id, guid) FROM stdin;
\.


--
-- Name: filestorage_completedgridinteractions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grid4data
--

SELECT pg_catalog.setval('filestorage_completedgridinteractions_id_seq', 1, false);


--
-- Data for Name: filestorage_filesse; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY filestorage_filesse (id, file_info_id, se_info_id) FROM stdin;
1	1	1
\.


--
-- Name: filestorage_filesse_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grid4data
--

SELECT pg_catalog.setval('filestorage_filesse_id_seq', 1, true);


--
-- Data for Name: filestorage_filestorageparameters; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY filestorage_filestorageparameters (id, filemetadata_id, filename, file_group_id, file_successfully_moved_to_se, file_successfully_uploaded_to_staging) FROM stdin;
1	3	testfile-2910-0937.tgz	1	f	f
\.


--
-- Name: filestorage_filestorageparameters_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grid4data
--

SELECT pg_catalog.setval('filestorage_filestorageparameters_id_seq', 1, true);


--
-- Data for Name: filestorage_filevo; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY filestorage_filevo (id, file_info_id, vo_info_id) FROM stdin;
1	1	1
\.


--
-- Name: filestorage_filevo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grid4data
--

SELECT pg_catalog.setval('filestorage_filevo_id_seq', 1, true);


--
-- Data for Name: filestorage_storageelementbasepath; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY filestorage_storageelementbasepath (id, location, storage_element, base_path) FROM stdin;
1	University of the Witwatersrand	se.core.wits.ac.za	/dpm/core.wits.ac.za/home
\.


--
-- Name: filestorage_storageelementbasepath_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grid4data
--

SELECT pg_catalog.setval('filestorage_storageelementbasepath_id_seq', 1, true);


--
-- Data for Name: filestorage_storedfilemetadata; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY filestorage_storedfilemetadata (id, title, description, subject, publisher, created_at, updated_at, type, format, identifier, source, language, relation, coverage, rights, creator_id) FROM stdin;
1	testdata-2910-0931	lorem ipsum	testing	testing	2014-10-29 09:32:13.765545+01	2014-10-29 09:32:13.76566+01	testing	testing	testing	testing	testing	testing	testing	testing	1
2	testdata-2910-0936	lorem ipsum	testing	testing	2014-10-29 09:36:43.55827+01	2014-10-29 09:36:43.558317+01	testing	testing	testing	testing	testing	testing	testing	testing	1
3	testdata-2910-0936	lorem ipsum	testing	testing	2014-10-29 09:37:38.66627+01	2014-10-29 09:37:38.666319+01	testing	testing	testing	testing	testing	testing	testing	testing	1
\.


--
-- Name: filestorage_storedfilemetadata_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grid4data
--

SELECT pg_catalog.setval('filestorage_storedfilemetadata_id_seq', 3, true);


--
-- Data for Name: filestorage_uservo; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY filestorage_uservo (id, username_id, virtual_organisation_id) FROM stdin;
1	1	1
2	1	2
\.


--
-- Name: filestorage_uservo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grid4data
--

SELECT pg_catalog.setval('filestorage_uservo_id_seq', 2, true);


--
-- Data for Name: filestorage_virtualorganisation; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY filestorage_virtualorganisation (id, virtual_organisation) FROM stdin;
1	sagrid
2	awigen
3	cern
\.


--
-- Name: filestorage_virtualorganisation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grid4data
--

SELECT pg_catalog.setval('filestorage_virtualorganisation_id_seq', 3, true);


--
-- Data for Name: lfcinteract_newdata; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY lfcinteract_newdata (id, title, description, creator, subject, publisher, contributor, date, tpe, frmat, identifier, source, language, relation, coverage, rights) FROM stdin;
\.


--
-- Name: lfcinteract_newdata_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grid4data
--

SELECT pg_catalog.setval('lfcinteract_newdata_id_seq', 1, false);


--
-- Data for Name: proxy_robotproxydetails; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY proxy_robotproxydetails (id, server_name, priority, url, md5, voms_id, proxy_renewal, disable_voms_proxy, rfc_proxy) FROM stdin;
1	SAGrid Server	1	http://etoken.sagrid.ac.za:8082/eTokenServer/eToken/	bc779e33367eaad7882b9dfaa83a432c	1	f	f	f
2	INFN Server 1	2	http://etokenserver.ct.infn.it:8082/eTokenServer/eToken/	bc779e33367eaad7882b9dfaa83a432c	1	f	f	f
3	INFN Server 2	3	http://etokenserver2.ct.infn.it:8082/eTokenServer/eToken/	bc779e33367eaad7882b9dfaa83a432c	1	f	f	f
\.


--
-- Name: proxy_robotproxydetails_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grid4data
--

SELECT pg_catalog.setval('proxy_robotproxydetails_id_seq', 3, true);


--
-- Data for Name: south_migrationhistory; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY south_migrationhistory (id, app_name, migration, applied) FROM stdin;
1	djcelery	0001_initial	2014-10-29 09:28:07.377462+01
2	djcelery	0002_v25_changes	2014-10-29 09:28:08.187956+01
3	djcelery	0003_v26_changes	2014-10-29 09:28:08.376877+01
4	djcelery	0004_v30_changes	2014-10-29 09:28:08.432181+01
5	filestorage	0001_initial	2014-10-29 09:30:03.243226+01
6	lfcinteract	0001_initial	2014-10-29 09:30:20.346324+01
7	proxy	0001_initial	2014-10-29 09:30:32.135454+01
8	ssl_authenticate	0001_initial	2014-10-29 09:30:51.467352+01
\.


--
-- Name: south_migrationhistory_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grid4data
--

SELECT pg_catalog.setval('south_migrationhistory_id_seq', 8, true);


--
-- Data for Name: ssl_authenticate_userprofile; Type: TABLE DATA; Schema: public; Owner: grid4data
--

COPY ssl_authenticate_userprofile (id, user_id, institution, groups) FROM stdin;
\.


--
-- Name: ssl_authenticate_userprofile_id_seq; Type: SEQUENCE SET; Schema: public; Owner: grid4data
--

SELECT pg_catalog.setval('ssl_authenticate_userprofile_id_seq', 1, false);


--
-- Name: auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions_group_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_key UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission_content_type_id_codename_key; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_key UNIQUE (content_type_id, codename);


--
-- Name: auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups_user_id_group_id_key; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_key UNIQUE (user_id, group_id);


--
-- Name: auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions_user_id_permission_id_key; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_key UNIQUE (user_id, permission_id);


--
-- Name: auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: celery_taskmeta_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY celery_taskmeta
    ADD CONSTRAINT celery_taskmeta_pkey PRIMARY KEY (id);


--
-- Name: celery_taskmeta_task_id_key; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY celery_taskmeta
    ADD CONSTRAINT celery_taskmeta_task_id_key UNIQUE (task_id);


--
-- Name: celery_tasksetmeta_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY celery_tasksetmeta
    ADD CONSTRAINT celery_tasksetmeta_pkey PRIMARY KEY (id);


--
-- Name: celery_tasksetmeta_taskset_id_key; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY celery_tasksetmeta
    ADD CONSTRAINT celery_tasksetmeta_taskset_id_key UNIQUE (taskset_id);


--
-- Name: django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type_app_label_model_key; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_key UNIQUE (app_label, model);


--
-- Name: django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: django_site_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY django_site
    ADD CONSTRAINT django_site_pkey PRIMARY KEY (id);


--
-- Name: djcelery_crontabschedule_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY djcelery_crontabschedule
    ADD CONSTRAINT djcelery_crontabschedule_pkey PRIMARY KEY (id);


--
-- Name: djcelery_intervalschedule_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY djcelery_intervalschedule
    ADD CONSTRAINT djcelery_intervalschedule_pkey PRIMARY KEY (id);


--
-- Name: djcelery_periodictask_name_key; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY djcelery_periodictask
    ADD CONSTRAINT djcelery_periodictask_name_key UNIQUE (name);


--
-- Name: djcelery_periodictask_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY djcelery_periodictask
    ADD CONSTRAINT djcelery_periodictask_pkey PRIMARY KEY (id);


--
-- Name: djcelery_periodictasks_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY djcelery_periodictasks
    ADD CONSTRAINT djcelery_periodictasks_pkey PRIMARY KEY (ident);


--
-- Name: djcelery_taskstate_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY djcelery_taskstate
    ADD CONSTRAINT djcelery_taskstate_pkey PRIMARY KEY (id);


--
-- Name: djcelery_taskstate_task_id_key; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY djcelery_taskstate
    ADD CONSTRAINT djcelery_taskstate_task_id_key UNIQUE (task_id);


--
-- Name: djcelery_workerstate_hostname_key; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY djcelery_workerstate
    ADD CONSTRAINT djcelery_workerstate_hostname_key UNIQUE (hostname);


--
-- Name: djcelery_workerstate_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY djcelery_workerstate
    ADD CONSTRAINT djcelery_workerstate_pkey PRIMARY KEY (id);


--
-- Name: filestorage_activegridinteractions_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY filestorage_activegridinteractions
    ADD CONSTRAINT filestorage_activegridinteractions_pkey PRIMARY KEY (id);


--
-- Name: filestorage_completedgridinteractions_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY filestorage_completedgridinteractions
    ADD CONSTRAINT filestorage_completedgridinteractions_pkey PRIMARY KEY (id);


--
-- Name: filestorage_filesse_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY filestorage_filesse
    ADD CONSTRAINT filestorage_filesse_pkey PRIMARY KEY (id);


--
-- Name: filestorage_filestorageparameters_file_group_id_key; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY filestorage_filestorageparameters
    ADD CONSTRAINT filestorage_filestorageparameters_file_group_id_key UNIQUE (file_group_id);


--
-- Name: filestorage_filestorageparameters_filemetadata_id_key; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY filestorage_filestorageparameters
    ADD CONSTRAINT filestorage_filestorageparameters_filemetadata_id_key UNIQUE (filemetadata_id);


--
-- Name: filestorage_filestorageparameters_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY filestorage_filestorageparameters
    ADD CONSTRAINT filestorage_filestorageparameters_pkey PRIMARY KEY (id);


--
-- Name: filestorage_filevo_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY filestorage_filevo
    ADD CONSTRAINT filestorage_filevo_pkey PRIMARY KEY (id);


--
-- Name: filestorage_storageelementbasepath_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY filestorage_storageelementbasepath
    ADD CONSTRAINT filestorage_storageelementbasepath_pkey PRIMARY KEY (id);


--
-- Name: filestorage_storedfilemetadata_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY filestorage_storedfilemetadata
    ADD CONSTRAINT filestorage_storedfilemetadata_pkey PRIMARY KEY (id);


--
-- Name: filestorage_uservo_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY filestorage_uservo
    ADD CONSTRAINT filestorage_uservo_pkey PRIMARY KEY (id);


--
-- Name: filestorage_virtualorganisation_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY filestorage_virtualorganisation
    ADD CONSTRAINT filestorage_virtualorganisation_pkey PRIMARY KEY (id);


--
-- Name: lfcinteract_newdata_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY lfcinteract_newdata
    ADD CONSTRAINT lfcinteract_newdata_pkey PRIMARY KEY (id);


--
-- Name: proxy_robotproxydetails_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY proxy_robotproxydetails
    ADD CONSTRAINT proxy_robotproxydetails_pkey PRIMARY KEY (id);


--
-- Name: south_migrationhistory_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY south_migrationhistory
    ADD CONSTRAINT south_migrationhistory_pkey PRIMARY KEY (id);


--
-- Name: ssl_authenticate_userprofile_pkey; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY ssl_authenticate_userprofile
    ADD CONSTRAINT ssl_authenticate_userprofile_pkey PRIMARY KEY (id);


--
-- Name: ssl_authenticate_userprofile_user_id_key; Type: CONSTRAINT; Schema: public; Owner: grid4data; Tablespace: 
--

ALTER TABLE ONLY ssl_authenticate_userprofile
    ADD CONSTRAINT ssl_authenticate_userprofile_user_id_key UNIQUE (user_id);


--
-- Name: auth_group_name_like; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX auth_group_name_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX auth_group_permissions_group_id ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX auth_group_permissions_permission_id ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX auth_permission_content_type_id ON auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX auth_user_groups_group_id ON auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX auth_user_groups_user_id ON auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_permission_id ON auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX auth_user_user_permissions_user_id ON auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_like; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX auth_user_username_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- Name: celery_taskmeta_hidden; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX celery_taskmeta_hidden ON celery_taskmeta USING btree (hidden);


--
-- Name: celery_taskmeta_task_id_like; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX celery_taskmeta_task_id_like ON celery_taskmeta USING btree (task_id varchar_pattern_ops);


--
-- Name: celery_tasksetmeta_hidden; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX celery_tasksetmeta_hidden ON celery_tasksetmeta USING btree (hidden);


--
-- Name: celery_tasksetmeta_taskset_id_like; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX celery_tasksetmeta_taskset_id_like ON celery_tasksetmeta USING btree (taskset_id varchar_pattern_ops);


--
-- Name: django_admin_log_content_type_id; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX django_admin_log_content_type_id ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX django_admin_log_user_id ON django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX django_session_expire_date ON django_session USING btree (expire_date);


--
-- Name: django_session_session_key_like; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX django_session_session_key_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: djcelery_periodictask_crontab_id; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX djcelery_periodictask_crontab_id ON djcelery_periodictask USING btree (crontab_id);


--
-- Name: djcelery_periodictask_interval_id; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX djcelery_periodictask_interval_id ON djcelery_periodictask USING btree (interval_id);


--
-- Name: djcelery_periodictask_name_like; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX djcelery_periodictask_name_like ON djcelery_periodictask USING btree (name varchar_pattern_ops);


--
-- Name: djcelery_taskstate_hidden; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX djcelery_taskstate_hidden ON djcelery_taskstate USING btree (hidden);


--
-- Name: djcelery_taskstate_name; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX djcelery_taskstate_name ON djcelery_taskstate USING btree (name);


--
-- Name: djcelery_taskstate_name_like; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX djcelery_taskstate_name_like ON djcelery_taskstate USING btree (name varchar_pattern_ops);


--
-- Name: djcelery_taskstate_state; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX djcelery_taskstate_state ON djcelery_taskstate USING btree (state);


--
-- Name: djcelery_taskstate_state_like; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX djcelery_taskstate_state_like ON djcelery_taskstate USING btree (state varchar_pattern_ops);


--
-- Name: djcelery_taskstate_task_id_like; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX djcelery_taskstate_task_id_like ON djcelery_taskstate USING btree (task_id varchar_pattern_ops);


--
-- Name: djcelery_taskstate_tstamp; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX djcelery_taskstate_tstamp ON djcelery_taskstate USING btree (tstamp);


--
-- Name: djcelery_taskstate_worker_id; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX djcelery_taskstate_worker_id ON djcelery_taskstate USING btree (worker_id);


--
-- Name: djcelery_workerstate_hostname_like; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX djcelery_workerstate_hostname_like ON djcelery_workerstate USING btree (hostname varchar_pattern_ops);


--
-- Name: djcelery_workerstate_last_heartbeat; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX djcelery_workerstate_last_heartbeat ON djcelery_workerstate USING btree (last_heartbeat);


--
-- Name: filestorage_filesse_file_info_id; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX filestorage_filesse_file_info_id ON filestorage_filesse USING btree (file_info_id);


--
-- Name: filestorage_filesse_se_info_id; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX filestorage_filesse_se_info_id ON filestorage_filesse USING btree (se_info_id);


--
-- Name: filestorage_filevo_file_info_id; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX filestorage_filevo_file_info_id ON filestorage_filevo USING btree (file_info_id);


--
-- Name: filestorage_filevo_vo_info_id; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX filestorage_filevo_vo_info_id ON filestorage_filevo USING btree (vo_info_id);


--
-- Name: filestorage_storedfilemetadata_creator_id; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX filestorage_storedfilemetadata_creator_id ON filestorage_storedfilemetadata USING btree (creator_id);


--
-- Name: filestorage_uservo_username_id; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX filestorage_uservo_username_id ON filestorage_uservo USING btree (username_id);


--
-- Name: filestorage_uservo_virtual_organisation_id; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX filestorage_uservo_virtual_organisation_id ON filestorage_uservo USING btree (virtual_organisation_id);


--
-- Name: proxy_robotproxydetails_voms_id; Type: INDEX; Schema: public; Owner: grid4data; Tablespace: 
--

CREATE INDEX proxy_robotproxydetails_voms_id ON proxy_robotproxydetails USING btree (voms_id);


--
-- Name: auth_group_permissions_permission_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_permission_id_fkey FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_fkey FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions_permission_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_permission_id_fkey FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: content_type_id_refs_id_d043b34a; Type: FK CONSTRAINT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT content_type_id_refs_id_d043b34a FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: crontab_id_refs_id_286da0d1; Type: FK CONSTRAINT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY djcelery_periodictask
    ADD CONSTRAINT crontab_id_refs_id_286da0d1 FOREIGN KEY (crontab_id) REFERENCES djcelery_crontabschedule(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_content_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_fkey FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filestorage_filesse_file_info_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY filestorage_filesse
    ADD CONSTRAINT filestorage_filesse_file_info_id_fkey FOREIGN KEY (file_info_id) REFERENCES filestorage_filestorageparameters(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filestorage_filestorageparameters_file_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY filestorage_filestorageparameters
    ADD CONSTRAINT filestorage_filestorageparameters_file_group_id_fkey FOREIGN KEY (file_group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filestorage_filestorageparameters_filemetadata_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY filestorage_filestorageparameters
    ADD CONSTRAINT filestorage_filestorageparameters_filemetadata_id_fkey FOREIGN KEY (filemetadata_id) REFERENCES filestorage_storedfilemetadata(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filestorage_filevo_file_info_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY filestorage_filevo
    ADD CONSTRAINT filestorage_filevo_file_info_id_fkey FOREIGN KEY (file_info_id) REFERENCES filestorage_filestorageparameters(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filestorage_storedfilemetadata_creator_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY filestorage_storedfilemetadata
    ADD CONSTRAINT filestorage_storedfilemetadata_creator_id_fkey FOREIGN KEY (creator_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filestorage_uservo_username_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY filestorage_uservo
    ADD CONSTRAINT filestorage_uservo_username_id_fkey FOREIGN KEY (username_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: filestorage_uservo_virtual_organisation_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY filestorage_uservo
    ADD CONSTRAINT filestorage_uservo_virtual_organisation_id_fkey FOREIGN KEY (virtual_organisation_id) REFERENCES filestorage_virtualorganisation(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: group_id_refs_id_f4b32aac; Type: FK CONSTRAINT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT group_id_refs_id_f4b32aac FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: interval_id_refs_id_1829f358; Type: FK CONSTRAINT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY djcelery_periodictask
    ADD CONSTRAINT interval_id_refs_id_1829f358 FOREIGN KEY (interval_id) REFERENCES djcelery_intervalschedule(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: proxy_robotproxydetails_voms_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY proxy_robotproxydetails
    ADD CONSTRAINT proxy_robotproxydetails_voms_id_fkey FOREIGN KEY (voms_id) REFERENCES filestorage_virtualorganisation(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: se_info_id_refs_id_902d4ad8; Type: FK CONSTRAINT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY filestorage_filesse
    ADD CONSTRAINT se_info_id_refs_id_902d4ad8 FOREIGN KEY (se_info_id) REFERENCES filestorage_storageelementbasepath(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: ssl_authenticate_userprofile_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY ssl_authenticate_userprofile
    ADD CONSTRAINT ssl_authenticate_userprofile_user_id_fkey FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_40c41112; Type: FK CONSTRAINT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT user_id_refs_id_40c41112 FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: user_id_refs_id_4dc23c39; Type: FK CONSTRAINT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT user_id_refs_id_4dc23c39 FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: vo_info_id_refs_id_a26db25a; Type: FK CONSTRAINT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY filestorage_filevo
    ADD CONSTRAINT vo_info_id_refs_id_a26db25a FOREIGN KEY (vo_info_id) REFERENCES filestorage_virtualorganisation(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: worker_id_refs_id_6fd8ce95; Type: FK CONSTRAINT; Schema: public; Owner: grid4data
--

ALTER TABLE ONLY djcelery_taskstate
    ADD CONSTRAINT worker_id_refs_id_6fd8ce95 FOREIGN KEY (worker_id) REFERENCES djcelery_workerstate(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

