from __future__ import absolute_import

import logging, os

from celery import Celery, task
from settings.celery import app
from django.contrib.auth.decorators import login_required

#from celery.contrib.methods import task_method

import argparse, time
from django.conf import settings
import gfal2, sys
from filestorage.models import FileStorageParameters

#from celery.utils.log import get_task_logger

#logger = get_task_logger(__name__)
logger = logging.getLogger(__name__)

@app.task
def test_task():
    logger.info("tasks sleeping for 10 seconds")
    time.sleep(10)
    logger.info("task waking up")
    return {"status" : "ok", "task completed" : "yes"}


# class GridCommands(object):
#
#     def __init__(self, **kwargs):
#         if kwargs is not None:
#             for key in ('storage_server_uploadurl', 'local_uploaded_file_path', 'options', 'proxy_filepath'):
#                 logger.info("key is {0}".format(key))
#                 if key in kwargs:
#                     setattr(self, key, kwargs[key])
#                 else:
#                     setattr(self, key, '')
#                     logger.info("no incoming value for {0}".format(key))

#     def event_callback(self, event):
#         #print event
#         logger.info("[{0}], {1}, {2}, {3}".format(event.timestamp, event.domain, event.stage, event.description))
#
#     def monitor_callback(self, src, dst, average, instant, transferred, elapsed):
#         logger.info("[{0}] {1}MB ({2}KB/s)\r".format(elapsed, transferred / 1048576, average / 1024))
#         #print "[%4d] %.2fMB (%.2fKB/s)\r" % (elapsed, transferred / 1048576, average / 1024),
#         sys.stdout.flush()

def reformat_url_and_filepath(storage_server_uploadurl, local_uploaded_file_path):

    # need to clean the urls more intelligently, this is susceptible to breakage

    storage_server_uploadurl = "https" + storage_server_uploadurl[5:]
    logger.info("new storage_server_uploadurl...{0}".format(storage_server_uploadurl))
    local_uploaded_file_path = "file:/" + local_uploaded_file_path
    logger.info("new local_uploaded_file_path...{0}".format(local_uploaded_file_path))

    return str(storage_server_uploadurl), str(local_uploaded_file_path)



# @shared_task
# def mul(x, y):
#     return x * y
#
# @shared_task
# def xsum(numbers):
#     return sum(numbers)
#
# def event_callback(event):
#     #print event
#     print "[%s] %s %s %s" % (event.timestamp, event.domain, event.stage, event.description)
#
# def monitor_callback(src, dst, average, instant, transferred, elapsed):
#     print "[%4d] %.2fMB (%.2fKB/s)\r" % (elapsed, transferred / 1048576, average / 1024),
#     sys.stdout.flush()
#

#     @app.task
#     def create_directory(self):
#
#         return None

@app.task
def upload_file_garbage_cleaner(filepath, upload_status=True):

    # read from database perhaps ?
    logger.info("trying to delete local file that has been uploaded")
    if upload_status == True:
        try:
            os.remove(filepath)
            return 'file deleted'
        except Exception, e:
            logger.error("an error trying to remove temp uploaded file...{0}".format(e))
            return 'file not deleted'

@app.task
def create_group_directory(gridurl):

    ctx = gfal2.creat_context()
    #params = ctx.transfer_parameters()

    try:
        logger.info("attempting to create directory on grid storage element")
        logger.info("by user...{0}".format(os.system('id')))
        r = ctx.mkdir(gridurl, 0)
        logger.info("gfal2 r...{0}".format(r))
        return {"status" : "ok", "dir created" : "yes"}
    except Exception, e:
        logger.error(e)
        return {"status" : "failed", "dir created" : "no - {0}".format(e)}

@app.task
def remove_group_directory(gridurl):

    ctx = gfal2.creat_context()
    #params = ctx.transfer_parameters()

    try:
        logger.info("attempting to remove directory on grid storage element")
        logger.info("by user...{0}".format(os.system('id')))
        r = ctx.rmdir(gridurl)
        logger.info("gfal2 r...{0}".format(r))
        return {"status" : "ok", "dir removed" : "yes"}
    except Exception, e:
        logger.error(e)
        return {"status" : "failed", "dir removed" : "no - {0}".format(e)}


def event_callback(event):
    #print event
    print "[%s] %s %s %s" % (event.timestamp, event.domain, event.stage, event.description)

def monitor_callback(src, dst, average, instant, transferred, elapsed):
    print "[%4d] %.2fMB (%.2fKB/s)\r" % (elapsed, transferred / 1048576, average / 1024),
    sys.stdout.flush()

@app.task
def capture_file_transfer_to_se(metapk, uploaded_file_path):

    try:
        logger.info("trying to access dataset for save...{0}".format(metapk))
        file_moved_to_se = FileStorageParameters.objects.get(filemetadata_id=metapk)
        file_moved_to_se.file_successfully_moved_to_se = True
        file_moved_to_se.save()
        logger.info('file transfer to SE captured in DB')
        upload_file_garbage_cleaner(uploaded_file_path, upload_status=True)

        return 'OK'
    except Exception, e:
        logger.error("problem saving SE transfer to DB...{0}".format(e))
        return 'Error'

@app.task
def delete_file_or_directory(metapk=None, uploadurl=None):
    logger.info("task to delete file...{0}".format(uploadurl))

    try:
        ctx = gfal2.creat_context()
        r = ctx.rmdir(uploadurl)
        params = ctx.transfer_parameters()
        logger.info("gfal2 r...{0}".format(r))
        if r==0:
            try:
                # need to log grid interaction here
                logger.info("directory removed")

            except Exception, e:
                logger.info("directory not removed")

        return {"status" : "ok", "file uploaded" : "yes"}
    except Exception, e:
        logger.error(e)
        return {"status" : "failed", "file uploaded" : "no - {0}".format(e)}


@app.task
def copy_file(metapk=None, uploadurl=None, uploaded_file_path=None):
    logger.info("upload url...{0}".format(uploadurl))
    logger.info("filepath...{0}".format(uploaded_file_path))

#     Parse arguments
#
#     parser = argparse.ArgumentParser(usage = 'usage: %prog [options] source destination')
#     parser.add_argument('-s', '--source-space-token', dest = 'source_space_token',
#                         help = 'Source space token')
#     parser.add_argument('-d', '--dest-space-token', dest = 'dest_space_token',
#                         help = 'Destination space token')
#     parser.add_argument('-K', '--validate', dest = 'validate', action = 'store_true',
#                         help = 'Validate source and destination checksum',
#                         default = False)
#     parser.add_argument('-c', '--checksum', dest = 'checksum',
#                         help = 'Checksum (i.e. ADLER32:1234)')
#     parser.add_argument('-o', '--overwrite', dest = 'overwrite', action = 'store_true',
#                         help = 'Overwrite destination')
    #logger.info("number of args...{0}".format(len(args)))
    #if len(kwargs) >= 2:
    #logger.info("what type is kwargs...{0}".format(type(args)))
    #for kwargs in kwargs.items:#('uploadurl', 'local_uploaded_file_path'):
    #    uploadurl = args.get('uploadurl')
        #logger.info("uploadurl kwarg...{0}".format(uploadurl))
    #    filepath = args.get('local_uploaded_file_path')
        #logger.info("filepath kwarg...{0}".format(filepath))
    #else:
    #    uploadurl = ''
    #    filepath = ''
        #logger.info("no incoming value for {0}".format(kwargs))

#         for (key, value) in kwargs:#('uploadurl', 'local_uploaded_file_path', 'options', 'proxy_filepath'):
#             logger.info("key is {0}".format(key))
#             logger.info("and value is {0}".format(value))

    #if __name__ == '__main__':
#     if options:
#
#     # Set transfer parameters
#         params = ctx.transfer_parameters()
#         params.event_callback   = event_callback
#         params.monitor_callback = monitor_callback
#
#         if options.overwrite:
#             params.overwrite = True
#             print "Enabled overwrite"
#
#         if options.validate or options.checksum:
#             params.checksum_check = True
#             print "Enabled checksum check"
#
#         if options.checksum:
#             (alg, val) = options.checksum.split(':')
#             params.set_user_defined_checksum(alg, val)
#             print "User defined checksum: %s:%s" % params.get_user_defined_checksum()
#
#         if options.source_space_token:
#             params.src_spacetoken = options.source_space_token
#             print "Source space token: %s" % params.src_spacetoken
#
#         if options.dest_space_token:
#             params.dst_spacetoken = options.dest_space_token
#             print "Destination space token: %s" % params.dst_spacetoken
#     else:
#         params={'timeout': 0}

    ctx = gfal2.creat_context()
    params = ctx.transfer_parameters()
    #Five minutes timeout
    params.timeout = 300
    params.event_callback   = event_callback
    params.monitor_callback = monitor_callback
#     params['certificate'] = "--cert " + proxy_filepath
#     params['key'] = "--key "+ proxy_filepath
#
    new_uploadurl, new_uploaded_file_path = reformat_url_and_filepath(uploadurl, uploaded_file_path)
    logger.info("reformatted uploadurl...{0}".format(new_uploadurl))
    logger.info("reformatted local_uploaded_file_path...{0}".format(new_uploaded_file_path))
    #Instantiate gfal2

    # Keep in mind source and destination can have different protocols, and gfal2 will deal with it
    # (i.e. source can be file:/// and destination gsiftp://)

    try:
        logger.info("Attempting to copy file...{0}".format(new_uploaded_file_path))
        logger.info("to destination...{0}".format(new_uploadurl))
        logger.info("by user...{0}".format(os.system('id')))
        r = ctx.filecopy(params, new_uploaded_file_path, new_uploadurl)
        logger.info("gfal2 r...{0}".format(r))
        if r==0:
            try:
                # need to log grid interaction here
                capture_file_transfer_to_se(metapk, uploaded_file_path)

            except Exception, e:
                logger.error("SE transfer to DB was not saved")

        return {"status" : "ok", "file uploaded" : "yes"}
    except Exception, e:
        logger.error(e)
        return {"status" : "failed", "file uploaded" : "no - {0}".format(e)}
