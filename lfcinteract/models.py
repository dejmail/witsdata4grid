from django.db import models
from django.contrib import admin

class NewData(models.Model):
    title = models.CharField(max_length=150)
    description = models.TextField(max_length=750)    
    creator = models.CharField(max_length=50)
    subject = models.CharField(max_length=25)
    publisher = models.CharField(max_length=20)
    contributor = models.CharField(max_length=20)
    date = models.DateTimeField() 
    tpe = models.CharField(max_length=20)
    frmat = models.CharField(max_length=20)
    identifier = models.CharField(max_length=20)
    source = models.CharField(max_length=20)
    language = models.CharField(max_length=10)
    relation = models.CharField(max_length=20)
    coverage = models.CharField(max_length=20)
    rights = models.CharField(max_length=20)
    
    def __unicode__(self):
        return self.title
        
    
admin.site.register(NewData)