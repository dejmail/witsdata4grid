#!/usr/bin/python

import gfal2
import os
import sys
from string import printable 

'''
Created on 03 Sep 2013

@author: liam
'''

class ReadFile(object):

    ## main func
    if __name__ == '__main__':
        # comment for usage
        if(len(sys.argv) < 2):
            print "\nUsage\t %s [gfal_url] \n"%(sys.argv[0])
            print "    Example: %s lfn:/grid/dteam/myfile "%(sys.argv[0])
            print "             %s srm://myserver.com/myhome/myfile \n"%(sys.argv[0])
            os._exit(1)
            
        ctx = gfal2.creat_context()
        # open the file
        f = ctx.open(sys.argv[1], 'r')
        # read first 10 bytesthe max_size first bytes.
        content = f.read(10)
        # Hex dump
        str = []
        for byte in content:
            print "%2X " % ord(byte),
            if byte in printable:
                str.append(byte)
            else:
                str.append('.')
        print '\t', ' '.join(str)
    
    # no close needed, done automatically with the destruction of the file handle


class FileCopyRegister(object):
    
    def event_callback(event):
        #print event
        print "[%s] %s %s %s" % (event.timestamp, event.domain, event.stage, event.description)

    def monitor_callback(src, dst, average, instant, transferred, elapsed):
        print "[%4d] %.2fMB (%.2fKB/s)\r" % (elapsed, transferred / 1048576, average / 1024),
        sys.stdout.flush()
    
    if __name__ == '__main__':
        # Parse arguments
        parser = optparse.OptionParser(usage = 'usage: %prog [options] source destination')
        parser.add_option('-s', '--source-space-token', dest = 'source_space_token',
                            help = 'Source space token')
        parser.add_option('-d', '--dest-space-token', dest = 'dest_space_token',
                            help = 'Destination space token')
        parser.add_option('-K', '--validate', dest = 'validate', action = 'store_true',
                            help = 'Validate source and destination checksum',
                            default = False)
        parser.add_option('-c', '--checksum', dest = 'checksum',
                            help = 'Checksum (i.e. ADLER32:1234)')
        parser.add_option('-o', '--overwrite', dest = 'overwrite', action = 'store_true',
                            help = 'Overwrite destination')
    
        (options, args) = parser.parse_args()
        if len(args) != 2:
            parser.error('Incorrect number of arguments. Need a source and a destination.')
    
        source = args[0]
        dest   = args[1]
    
        print "Source:      %s" % source
        print "Destination: %s" % dest
    
        # Instantiate gfal2
        ctx = gfal2.creat_context()
    
        # Set transfer parameters
        params = ctx.transfer_parameters()
        params.event_callback   = event_callback
        params.monitor_callback = monitor_callback
    
        if options.overwrite:
            params.overwrite = True
            print "Enabled overwrite"
    
        if options.validate or options.checksum:
            params.checksum_check = True
            print "Enabled checksum check"
    
        if options.checksum:
            (alg, val) = options.checksum.split(':')
            params.set_user_defined_checksum(alg, val)
            print "User defined checksum: %s:%s" % params.get_user_defined_checksum()
    
        if options.source_space_token:
            params.src_spacetoken = options.source_space_token
            print "Source space token: %s" % params.src_spacetoken
        if options.dest_space_token:
            params.dst_spacetoken = options.dest_space_token
            print "Destination space token: %s" % params.dst_spacetoken
    
        # Five minutes timeout
        params.timeout = 300
    
        # Copy!
        # Keep in mind source and destination can have different protocols,
        # and gfal2 will deal with it
        # (i.e. source can be file:/// and destination gsiftp://)
        try:
            r = ctx.filecopy(params, source, dest)
            print "Copy succeeded!"
        except Exception, e:
            print "Copy failed: %s" % str(e)
            sys.exit(1)


class CopyRegister(object):
    
        def __init__(self):            
            '''
            Constructor
            '''
        def get_guid(self, parameters):

            import sys
            import traceback
            import lfc2 as lfc
            
            
            """
            # stat an existing entry in the LFC and print the GUID
            """
            
            name = "/dpm/core.wits.ac.za/home/sagrid/liam/admixture/admixture_data.tar.gz"
            
            try:
                stat = lfc.lfc_statg(name,"")
            except Exception:
                traceback.print_exc()
                sys.exit(1)
            
            guid = stat.guid
            print "The GUID for " + name + " is " + guid
            
        def list_replicas(self, parameters):

            import lfc
            
            """
            # list the replicas of a given entry, starting from the GUID
            """
            guid = "efef5eaf-37d9-4774-9582-67fdab800f95"
            
            listp = lfc.lfc_list()
            flag = lfc.CNS_LIST_BEGIN
            
            print "Listing replicas for GUID " + guid
            
            num_replicas=0
            
            while(1):
                res = lfc.lfc_listreplica("",guid,flag,listp)
                flag = lfc.CNS_LIST_CONTINUE
            
                if res == None:
                    break
                else:
                    rep_name = res.sfn
                    print "Replica: " + rep_name
                    num_replicas = num_replicas + 1
            
            lfc.lfc_listreplica("",guid,lfc.CNS_LIST_END,listp)
            print "Found " + str(num_replicas) + " replica(s)"


class InputData(object):
    
    def __init__(self):
    
def input_form(request):
    from django.shortcuts import render_to_response
    
    return render_to_response(request, 'input_form.html')

def search(request):
    from django.shortcuts import HttpResponse

    if 'q' in request.GET:
        message = 'You search for: %r' % request.GET['q']
    else:
        message = 'You submitted and empty form.'
    return HttpResponse(message)
    
    
    