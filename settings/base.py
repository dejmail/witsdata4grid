"""Base settings shared by all environments"""
# Import global settings to make it easier to extend settings.
from django.conf.global_settings import *
from django.core.exceptions import ImproperlyConfigured
from django import template
import os

#==============================================================================
# Generic Django project settings
#==============================================================================

SITE_ID = 1
# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
TIME_ZONE = 'Africa/Johannesburg'
USE_TZ = True
USE_I18N = True
USE_L10N = True
LANGUAGE_CODE = 'en'
LANGUAGES = (
    ('en', 'English'),
)

msg ="Set the %s environment variable"

def get_env_variable(var_name):

    try:
        return os.environ[var_name]
    except KeyError:
        error_msg = msg % var_name
        raise ImproperlyConfigured(error_msg)

# Make this unique, and don't share it with anybody.
#SECRET_KEY = '20+l&(r@@=kpny6o@@(&@knc(70h!&x6o-ed$&eku4heawc1_n'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.admin',
    'django.contrib.admindocs',
    # extra installed apps
    'filestorage',
    'lfcinteract',
    'proxy',
    'ssl_authenticate',
    'south',
    'celery',
    'djcelery',
    'guardian',
    #'testing',
    )

#import django.core.management.setup_environ
#import os

#os.environ.setdefault("DJANGO_SETTINGS_MODULE", "fooproject.settings")
#from django.conf import settings

#DJANGO_SECRET_KEY='h0&ka83ej9xx!e5sm7)+6c9$rbgo_)rndt)uyzkbu(5_o)uv^r'

#SECRET_KEY='h0&ka83ej9xx!e5sm7)+6c9$rbgo_)rndt)uyzkbu(5_o)uv^r'
#from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS


# msg ="Set the %s environment variable"
#
# def get_env_variable(var_name):
#
#     try:
#         return os.environ[var_name]
#     except KeyError:
#         error_msg = msg % var_name
#         raise ImproperlyConfigured(error_msg)

#SECRET_KEY = get_env_variable("DJANGO_SECRET_KEY")

#==============================================================================
# Middleware
#==============================================================================

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    #'django.contrib.auth.middleware.RemoteUserMiddleware',
    'ssl_authenticate.base.SSLClientAuthMiddleware',
    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)


#==============================================================================
# Auth / security
#==============================================================================

AUTHENTICATION_BACKENDS = ('django.contrib.auth.backends.ModelBackend',
                            'ssl_authenticate.base.SSLClientAuthBackend',
                            'django.contrib.auth.backends.ModelBackend', # this is default
                            'guardian.backends.ObjectPermissionBackend',
                           )

ANONYMOUS_USER_ID = -1


#==============================================================================
# Miscellaneous project settings
#==============================================================================

# Python dotted path to the WSGI application used by Django's runserver.

WSGI_APPLICATION = 'apache.wsgi.application'

# for file transfers downloads
#SERVE_FILE_BACKEND = 'filetransfers.backends.xsendfile.serve_file'

# for file transfers uploads
#PREPARE_UPLOAD_BACKEND = 'filetransfers.backends.default.prepare_upload'
#SERVE_FILE_BACKEND = 'filetransfers.backends.default.serve_file'

MAX_UPLOAD_SIZE = 2147483647  # 2GB

PROXY_PATH = '/tmp/x509up_u0'


#==============================================================================
# Third party app settings
#==============================================================================

AUTOCREATE_VALID_SSL_USERS = True

