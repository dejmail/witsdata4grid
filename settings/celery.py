from __future__ import absolute_import

import os, socket
from celery import Celery
from django.conf import settings

import djcelery
djcelery.setup_loader()

#set the default Django settings module for the 'celery' program.

#if not settings.configured :
#    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'l.settings')

if socket.gethostname() == 'blueice':
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings.local')
#     #CELERYD_CHDIR="/opt/code_home/django/witsdata4grid"
# 
#     app = Celery('witsdata4grid')
    
#     'witsdata4grid',
#     broker='amqp://guest@localhost//',
#     backend='amqp://',
#     include=['lfcinteract.tasks', 'proxy.tasks',]             
                 
    
    
if socket.gethostname() == 'ray.core.wits.ac.za':
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'settings.staging')
    #CELERYD_CHDIR="/opt/python/witsdata4grid"
    
app = Celery('witsdata4grid')    

app.config_from_object('django.conf:settings')

#app.conf.update(CELERY_TASK_RESULT_EXPIRES=10.0,)
#app.config_from_object('django.conf:settings')

app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

if __name__ == '__main__':
    app.start()

@app.task(bind=True)
def debug_task(self):
    print('Request: {0!r}'.format(self.request))


