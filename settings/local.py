from base import *
import os, sys
from django.conf import settings
from kombu import serialization

DEBUG = True
TEMPLATE_DEBUG = True

DEFAULT_CONFIG = {
    'INTERCEPT_REDIRECTS': False,
}

ADMINS = (
    ('Liam Thompson', 'dejmail@gmail.com'),
)

MANAGERS = ADMINS

EMAIL_HOST = "localhost"
EMAIL_PORT = 1025

DATABASES = {
 "default": {
 "ENGINE": "django.db.backends.postgresql_psycopg2",
 "NAME": "grid4data",
 "USER": "grid4data",
 "PASSWORD": "grid4data",
 "HOST": "127.0.0.1",
 "PORT": "",
 }
}

INSTALLED_APPS += ('mod_wsgi.server',)

#==============================================================================
# Project URLS and media settings
#==============================================================================

#FORCE_SCRIPT_NAME = '/django/'
#SUB_SITE = 'django/'
#LOGIN_URL = '/accounts/login/'


INSTALLED_APPS += ("django.contrib.staticfiles","debug_toolbar",)

#SECRET_KEY = "h0&ka83ej9xx!e5sm7)+6c9$rbgo_)rndt)uyzkbu(5_o)uv^r"

DJANGO_SECRET_KEY='h0&ka83ej9xx!e5sm7)+6c9$rbgo_)rndt)uyzkbu(5_o)uv^r'
 
SECRET_KEY='h0&ka83ej9xx!e5sm7)+6c9$rbgo_)rndt)uyzkbu(5_o)uv^r'

template.add_to_builtins('django.contrib.staticfiles.templatetags.staticfiles')

#DJANGO_SECRET_KEY = get_env_variable("DJANGO_SECRET_KEY")

#get_env_variable("DJANGO_SECRET_KEY")

INTERNAL_IPS = ("127.0.0.1",)

MIDDLEWARE_CLASSES += ("debug_toolbar.middleware.DebugToolbarMiddleware", )


#==============================================================================
# WSGI Debugging
#===============================================================================

# print "__name__ =", __name__
# print "__file__ =", __file__
# print "os.getpid() =", os.getpid()
# print "os.getcwd() =", os.getcwd()
# print "os.curdir =", os.curdir
# print "sys.path =", repr(sys.path)
# print "sys.modules.keys() =", repr(sys.modules.keys())
# print "sys.modules.has_key('witsdata4grid') =", sys.modules.has_key('witsdata4grid')
# if sys.modules.has_key('witsdata4grid'):
#     print "sys.modules['witsdata4grid'].__name__ =", sys.modules['witsdata4grid'].__name__
#     print "sys.modules['witsdata4grid'].__file__ =", sys.modules['witsdata4grid'].__file__
#     print "os.environ['DJANGO_SETTINGS_MODULE'] =", os.environ.get('DJANGO_SETTINGS_MODULE', None)
 
#==============================================================================
# Project URLS and media settings
#==============================================================================

SETTINGS_DIR = os.path.dirname(__file__)
PROJECT_PATH = os.path.join(SETTINGS_DIR, os.pardir)
PROJECT_PATH = os.path.abspath(PROJECT_PATH)

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
           
DJANGO_ROOT = os.path.abspath(os.path.dirname(__name__))
SITE_ROOT = os.path.abspath(os.path.dirname(__name__))

ROOT_URLCONF = 'urls'

LOGIN_URL = 'accounts/login'
LOGOUT_URL = 'accounts/logout'
LOGIN_REDIRECT_URL = '/'

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(PROJECT_PATH, 'media')

STATIC_URL = '/static/'
STATIC_ROOT =  os.path.join(DJANGO_ROOT, 'htdocs')
STATICFILES_DIRS = (os.path.join(DJANGO_ROOT, 'static'),)

#GRIDBOX_URL = 'https://ray.core.wits.ac.za/gridbox/'
GRIDBOX_URL = 'http://gridbox.localhost:8080/'

# Uploads Directory
UPLOAD_DIRECTORY = os.path.join(MEDIA_ROOT, 'uploads')
CHUNKS_DIRECTORY = os.path.join(MEDIA_ROOT, 'chunks')


#==============================================================================
# Celery Settings
#==============================================================================

#BROKER_URL = 'amqp://guest@localhost:5672'
#BROKER_HOST = 'localhost'
#BROKER_PORT = 5672
#BROKER_VHOST = '/witsdata4grid'
#BROKER_USER = 'witsdata4grid'
#BROKER_PASSWORD = 'witsdata4grid'

CELERY_RESULT_BACKEND = 'amqp://'
#CELERYD_LOG_FILE=os.path.join(PROJECT_PATH, 'logs/celery.log')
CELERY_TIMEZONE='Africa/Johannesburg'
CELERY_ENABLE_UTC=True
CELERY_TASK_SERIALIZER='json'
CELERY_ACCEPT_CONTENT=['json']  # Ignore other content
CELERY_RESULT_SERIALIZER='json'
CELERY_RESULT_BACKEND='djcelery.backends.database:DatabaseBackend'
CELERY_RESULT_BACKEND='djcelery.backends.cache:CacheBackend'
#CELERY_IMPORTS = ('lfcinteract.tasks','proxy.tasks')
#CELERYD_HIJACK_ROOT_LOGGER = False

#==============================================================================
# Templates
#==============================================================================

TEMPLATE_DIRS = (os.path.join(BASE_DIR, 'templates'),
                 '/opt/code_home/django/witsdata4grid/templates/',)
                 #'/opt/code_home/django/witsdata4grid/filestorage/templates',)
                #'/opt/code_home/django/witsdata4grid/templates/accounts')
                 #(os.path.join(BASE_DIR, 'templates')))


TEMPLATE_CONTEXT_PROCESSORS += ('django.contrib.auth.context_processors.auth',
                                "django.core.context_processors.debug",
                                "django.core.context_processors.i18n",
                                "django.core.context_processors.media",
                                "django.core.context_processors.static",
                                "django.core.context_processors.tz",
                                "django.core.context_processors.request",
                                "django.contrib.messages.context_processors.messages"
                                )

TEMPLATE_LOADERS += ('django.template.loaders.filesystem.Loader',
                     'django.template.loaders.app_directories.Loader',
                     )

#==============================================================================
# Logging settings
#==============================================================================

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format': '%(asctime)s | %(levelname)s | %(name)s | %(module)s | %(message)s',
            'datefmt' : '%Y-%m-%d %H:%M'
        },
    },
    'handlers': {
        'default': {
            'level':'DEBUG',
            'class':'logging.handlers.RotatingFileHandler',
            'filename': '/opt/code_home/django/witsdata4grid/logs/django.log',
            'maxBytes': 1024*1024*5, # 5 MB
            'backupCount': 5,
            'formatter':'standard',
        },
        'null': {
            'level': 'DEBUG',
            'class':'django.utils.log.NullHandler',
            },  
        'request_handler': {
                'level':'DEBUG',
                'class':'logging.handlers.RotatingFileHandler',
                'filename': '/opt/code_home/django/witsdata4grid/logs/django_request.log',
                'maxBytes': 1024*1024*5, # 5 MB
                'backupCount': 5,
                'formatter':'standard',
        },
        'celery': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': '/opt/code_home/django/witsdata4grid/logs/celery.log',
            'formatter': 'standard',
            'maxBytes': 1024 * 1024 * 5,  # 100 mb
        },
    },
    'loggers': {

        '': {
            'handlers': ['default'],
            'level': 'DEBUG',
            'propagate': True
        },
        'django.request': { # Stop SQL debug from logging to main logger
            'handlers': ['request_handler'],
            'level': 'DEBUG',
            'propagate': False
        },
        'celery': {
            'handlers': ['celery'],
            'level': 'DEBUG',
            },
        'django.db.backends': {
            'handlers': ['null'],  # Quiet by default!
            'propagate': False,
            'level':'INFO',
        },
    }
}
