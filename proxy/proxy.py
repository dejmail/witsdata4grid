
class ProxyCredentials():
    
    def __init__(self):            
        '''
            Constructor
        '''

    def determine_linux_userid(self):
        '''
        VOMS proxy generates a proxy based on the Linux user ID under /tmp/x509up_u< UID >        
        '''
        import subprocess
        self.userid = subprocess.check_output("id -u", shell=True).strip("\n")
        
        return self.userid
    
    def request_proxy_from_etoken_server(self):
        import requests, logging
        
        self.logger = logging.getLogger(__name__)
        
        self.payload = {'voms' : 'eumed:eumed', 'proxy-renewal': 'false', 'disable-voms-proxy' : 'false', 'rfc-proxy' : 'false'}        
        self.proxy_data = requests.get("http://etoken.sagrid.ac.za:8082/eTokenServer/eToken/332576f78a4fe70a52048043e90cd11f", params=self.payload)
        
        if self.proxy_data.status_code == 200:
            self.logger.info("Proxy status OK - " + str(self.proxy_data.status_code))
            return self.proxy_data
        else: 
            return None
        
    def save_proxy_to_temporary_file(self):
        
        '''
        Generate a file in /tmp containing the proxy information from the etoken.sagrid server. When the proxy file is closed, 
        it is automatically deleted.
        '''
        
        import os, tempfile, logging
        
        self.logger = logging.getLogger(__name__)
        
        self.temp_filepath = tempfile.gettempdir()
        self.logger.info("Temp directory = " + self.temp_filepath)
        self.filename_prefix = "x509up_u"
        self.logger.info("Filename prefix = " + self.filename_prefix)
        self.filename_suffix = self.determine_linux_userid()
        self.logger.info("Filename suffix = " + self.filename_suffix)
        self.proxy_data = self.request_proxy_from_etoken_server()        
        
        self.proxy_fd, self.proxy_name = tempfile.mkstemp(self.filename_suffix, self.filename_prefix, self.temp_filepath, text=True)
        
        self.f = os.fdopen(self.proxy_fd, 'w+')
        self.f.write(self.proxy_data.text)
        
        return True

    def delete_temporary_proxyfile(self):
        
        '''
        Delete the temporary proxy file
        '''
        
        self.f.close()
        
        return None
    
        
        
    
    

