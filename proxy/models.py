from django.db import models
from django.contrib import admin
from filestorage.models import VirtualOrganisation

class RobotProxyDetails(models.Model):
    
    server_name = models.CharField(max_length=16)
    priority = models.IntegerField()
    url = models.URLField()
    md5 = models.CharField(max_length=32)
    voms = models.ForeignKey(VirtualOrganisation)
    proxy_renewal = models.BooleanField()
    disable_voms_proxy = models.BooleanField()
    rfc_proxy = models.BooleanField()
    
    def __unicode__(self):
        return self.server_name
    
admin.site.register(RobotProxyDetails)
