from __future__ import absolute_import

from django.shortcuts import *
import requests
import logging
import os
from django.http import HttpResponseServerError

from proxy.models import RobotProxyDetails

from django.db import connection

from celery import Celery, shared_task, task
from settings.celery import app

import M2Crypto

from celery.utils.log import get_task_logger
from settings.base import PROXY_PATH
logger = get_task_logger(__name__)

def delete_proxy_file():
    try:
        os.remove(PROXY_PATH)
    except Exception, e:
        logger.error('error deleting proxy file...{0}'.format(e))
    return None

def write_proxy_file(PROXY_PATH, certificate_text):

    try:
        proxy = open(PROXY_PATH, "w");
        proxy.write(certificate_text)
        proxy.close()
        os.chmod(PROXY_PATH, 0600)
        #os.chown(file_path, 0, 0)
        logger.info("user running this process...{0}".format(os.system('id')))
        logger.info("proxy file written to {0}".format(PROXY_PATH))
        return True
    except IOError as e:
        logger.error("proxy IOError...{0}".format(e))
        return False
    except OSError as e:
        logger.error("proxy OSError...{0}".format(e))
        return False

def concatenate_proxy_info_into_url(url_parts, vo):

    logger = logging.getLogger(__name__)

    server_url = url_parts.url + \
                 url_parts.md5 + \
                 '?voms=' + vo + ':' + vo + \
                 '&proxy-renewal=' + str(url_parts.proxy_renewal) + \
                 '&disable-voms-proxy=' + str(url_parts.disable_voms_proxy) + \
                 '&rfc-proxy=' + str(url_parts.rfc_proxy)

    logger.info("proxy server url type...{0}".format(type(server_url)))

    return server_url

@app.task
def check_proxy_health(vo=None):

    from django.utils import timezone

    logger = logging.getLogger(__name__)
    if os.path.exists(PROXY_PATH):
        try:
            certificate = M2Crypto.X509.load_cert(PROXY_PATH, format=1)
            cert_time_left = certificate.get_not_after()
            timedelta = cert_time_left.get_datetime() - timezone.now()
            logger.info("there is {0} time left on the proxy".format(timedelta))
            if timedelta.seconds < 7200 or timedelta.days < 1:
                logger.info("too little time left on proxy...renewing")
                delete_proxy_file()
                get_proxy.run()
            else:
                logger.info("proxy still valid")
        except Exception, e:
            logger.error('error checking proxy validity...{0}'.format(e))
    else:
        get_proxy.run()

    return None

@app.task
def get_proxy(vo='sagrid'):

    logger = logging.getLogger(__name__)

    """
    Virtual organisation is defaulted to sagrid, could be change at a later stage if different VOs needed.


    Fields in the model
    url, md5, disable-voms-proxy, rfc-proxy, proxy-renewal

    """

    robot_servers = RobotProxyDetails.objects.filter(voms_id__virtual_organisation=vo)
    logger.info('proxies with certs for vo = %s,', robot_servers.count())

    for etoken_server in robot_servers:
        #url_parts = etoken_server.values('url','md5', 'voms', 'disable_voms_proxy', 'rfc_proxy', 'proxy_renewal')
        server_url = concatenate_proxy_info_into_url(etoken_server, vo)
        logger.info("proxy server url...{0}".format(server_url))

        try:
            etoken_server_request = requests.get(server_url)
            logger.info("etoken server response...{0}".format(etoken_server_request.status_code))
            if etoken_server_request.status_code == 200 :

                if write_proxy_file(PROXY_PATH, etoken_server_request.text) == True:
                    return "proxy written to file"
                else:
                    return "unable to write to file"

            else:
                logger.warning("etoken sent a bad response")

        except Exception, e:
            logger.error("connection error to proxy server...{0}".format(e))


