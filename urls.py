from django.conf import settings
from django.conf.urls import patterns, include, url, static
from django.views.generic import TemplateView, RedirectView
#from django.conf import settings
# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

#from ssl_authenticate.views import login
#from gridbox import dm

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'temp.views.home', name='home'),
    # url(r'^temp/', include('temp.foo.urls')),

    #send root url to login
    url(r'^$', 'ssl_authenticate.views.certificate_login', name='index'),
    #TemplateView.as_view(template_name='index.html'), name="home"),

    url(r'^contact/', TemplateView.as_view(template_name='base/contact.html'), name="contact"),
    url(r'^about/', TemplateView.as_view(template_name='base/about.html'), name="about"),
    url(r'^howto/', TemplateView.as_view(template_name='base/howto.html'), name="howto"),
    # Uncomment the admin/doc line below to enable admin documentation:

    # admin urls
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    #url(r'^reset/', 'filestorage.reset.reset'),
    #user authentication urls
    #url(r'^accounts/auth/$', 'testing.views.auth_view'),
    url(r'^accounts/certlogin/$', 'ssl_authenticate.views.certificate_login', name="certlogin"),
    url(r'^accounts/simplelogin/$', 'ssl_authenticate.views.simple_login', name="simplelogin"),
    url(r'^accounts/logout/$', 'ssl_authenticate.views.logout', name="logout"),
    url(r'^accounts/loggedin/$', 'ssl_authenticate.views.logged_in', name="loggedin"),
    url(r'^accounts/invalid/$', 'ssl_authenticate.views.invalid_login', name="invalid"),
    url(r'^accounts/profile/$', 'ssl_authenticate.views.profile', name="profile"),

    # registration of new users
    url(r'^accounts/register/$', 'ssl_authenticate.views.register_user', name='register'),
    url(r'^accounts/register_success/$', 'ssl_authenticate.views.register_success', name='registersuccess'),

    # data manipulation urls
    #url(r'^data/$', 'filestorage.views.alldata'),
    url(r'^data/all/$', 'filestorage.views.alldata', name="viewalldata"),
    url(r'^data/groupall/$', 'filestorage.views.group_data', name="viewgroupdata"),
    url(r'^data/create/$', 'filestorage.views.create', name="datacreate"),
    url(r'^data/choose/$', 'filestorage.views.choose_data', name="choose_data"),
    url(r'^data/edit/(\d+)/$', 'filestorage.views.edit_data', name="edit_data"),
    url(r'^data/incomplete/$', 'filestorage.views.incomplete_data', name="nofile"),
    url(r'^data/fileupload/$', 'filestorage.views.reserve_fileupload_space', name="fileupload"),
    url(r'^data/download/$', 'filestorage.views.file_download', name="download"),
    url(r'^data/upload/$', 'filestorage.views.file_upload', name="upload"),
    url(r'^data/segment_upload/$', 'filestorage.views.file_segment_upload', name="segment_upload"),
    url(r'^data/get/(?P<data_id>\d+)/$', 'filestorage.views.data_display', name="datadisplay"),
    url(r'^data/success/$', 'filestorage.views.upload_success', name="upload_success"),
)

urlpatterns += static.static('/browse/', document_root=settings.UPLOAD_DIRECTORY)
