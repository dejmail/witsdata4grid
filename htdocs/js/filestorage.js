jQuery.noConflict();
jQuery( document ).ready(function() {
     
        function _ajax_request(url, data, callback, type, method) {
        if ($.isFunction(data)) {
            callback = data;
            data = {};
        }
        return jQuery.ajax({
            type: method,
            url: url,
            data: data,
            success: callback,
            dataType: type
            });
        }
    
      jQuery.extend({
        put: function(url, data, callback, type) {
            return _ajax_request(url, data, callback, type, 'PUT');
        },
        delete_: function(url, data, callback, type) {
            return _ajax_request(url, data, callback, type, 'DELETE');
        }
    });
        
    jQuery(':file').change(function(){
        var file = this.files[0];
        var name = file.name;
        var size = file.size;
        var type = file.type;
   });
    
	jQuery('#submitFile').onclick(function() {
		jQuery.ajax({
		    url : $("fileform").attr("action"),
			type : "PUT",
			contentType: false,
            processData: false,
			data : function() {
			    var data = new FormData();
			    data.append("chosenFile", jQuery("#chosenFile").get(0).files[0]);
			    return data;
			}(),
			error : function (_, textStatus, errorThrown) {
			    alert("Error");
			    console.log("error");
			    console.log(textStatus, errorThrown);
			},
			success : function (response, textStatus) {
                alert("Success");
                console.log("success");
                console.log(response, textStatus);
            }
        });
	});
});

